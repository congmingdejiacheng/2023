#define _CRT_SECURE_NO_WARNINGS 1
#include<malloc.h>

#include<string.h>

#include<stdio.h>


typedef struct {

    char ch;

    int index;

} CharIndexMapping;


typedef struct {

    unsigned int weight;

    unsigned int parent, lchild, rchild;

}HTNode, * HuffmanTree;    //动态分配数组存储哈夫曼树

typedef char** HuffmanCode;    //动态分配数组存储哈夫曼编码表



void Select(HuffmanTree HT, int i, int* s1, int* s2) {

    //在HT[0..i-1]找出parent为0且weight最小的两个结点，其序号分别为s1和s2

    int k, j = 0;//k控制HT数组，j保证比较进行下去

    unsigned int min1, min2;

    for (k = 0; k <= i - 1; ++k) {

        if (HT[k].parent == 0) {

            if (j == 0) {

                min1 = HT[k].weight;

                *s1 = k;

            }

            else {//始终能保证min1是最小的，min2第二小

                if (HT[k].weight < min1)

                {

                    min2 = min1;  *s2 = *s1;

                    min1 = HT[k].weight;  *s1 = k;

                }

                else if (HT[k].weight >= min1 && HT[k].weight < min2) {

                    min2 = HT[k].weight;  *s2 = k;

                }

            }

            ++j;

        }

    }

}



void CreateHuffmanTree(HuffmanTree& HT, int* w, int n) {

    if (n <= 1)return;

    int m = 2 * n - 1;    //赫夫曼树有m个结点,m控制数组开多大，叶子节点有n个，哈夫曼总结点数为2n-1个

    HT = (HuffmanTree)malloc(m * sizeof(HTNode));

    HuffmanTree p = HT;

    int i;

    for (i = 0; i < n; ++i, ++p, ++w) {//给定的权值输入进去

        p->weight = *w;

        p->parent = 0; p->lchild = 0; p->rchild = 0;

    }

    for (; i < m; ++i, ++p) {//其余全赋为0，此时i接着上一个for循环下来的i继续

        p->weight = 0;

        p->parent = 0; p->lchild = 0; p->rchild = 0;

    }



    for (i = n; i < m; ++i) {    //建哈夫曼树

    //在HT[0..i-1]选择parent为0且weight最小的两个结点，其序号分别为s1和s2

        int s1, s2;

        Select(HT, i, &s1, &s2);

        HT[s1].parent = i;  HT[s2].parent = i;

        HT[i].lchild = s1;  HT[i].rchild = s2;

        HT[i].weight = HT[s1].weight + HT[s2].weight;//从下往上建立，导致头节点是HT[2n-2]

    }

}



void HuffmanCoding(HuffmanTree& HT, HuffmanCode& HC, int* w, int n) {

    //w存放n个字符权值（均>0），构造哈夫曼树HT，并求n个字符的哈夫曼编码HC

    int i;

    //---从叶子到根逆向求每个字符的哈夫曼编码---

    HC = (HuffmanCode)malloc(n * sizeof(char*));

    char* cd = (char*)malloc(n * sizeof(char));

    cd[n - 1] = '\0';

    int start, f;

    unsigned int c;

    for (i = 0; i < n; ++i) {

        start = n - 1;

        for (c = i, f = HT[i].parent; f != 0; c = f, f = HT[f].parent) {

            if (HT[f].lchild == c) cd[--start] = '0';

            else cd[--start] = '1';

        }

        HC[i] = (char*)malloc((n - start) * sizeof(char));

        strcpy(HC[i], &cd[start]);

    }

    free(cd);

}


// 解码函数

void HuffmanDecoding(HuffmanTree HT, HuffmanCode HC, int n) {

    char code[1024]; // 假设编码不会超过1024位

    printf("请输入需要解译的代码：\n");

    scanf("%s", code); // 读取编码字符串



    int m = 2 * n - 1; // 哈夫曼树总节点数

    int p = m - 1; // 从根节点开始

    for (int i = 0; code[i] != '\0'; ++i) { // 遍历整个编码字符串

        if (code[i] == '0') {

            p = HT[p].lchild; // 向左走

        }
        else {

            p = HT[p].rchild; // 向右走

        }



        if (HT[p].lchild == 0 && HT[p].rchild == 0) { // 如果是叶子节点

            printf("%c", (char)(p + 'A' - 1)); // 输出对应字符假设字符按A、B..排序

            p = m - 1; // 返回根节点

        }

    }

    printf("\n");

}


// 解码函数

void HuffmanDecoding(HuffmanTree HT, CharIndexMapping* mappings, int n) {

    char code[1024]; // 假设编码不会超过1024位

    printf("4.解码功能\n\n请输入需要解译的代码：\n");

    scanf("%s", code); // 读取编码字符串



    int m = 2 * n - 1; // 哈夫曼树总节点数

    int p = m - 1; // 从根节点开始

    for (int i = 0; code[i] != '\0'; ++i) { // 遍历整个编码字符串

        if (code[i] == '0') {

            p = HT[p].lchild; // 向左走

        }
        else {

            p = HT[p].rchild; // 向右走

        }



        if (HT[p].lchild == 0 && HT[p].rchild == 0) { // 如果是叶子节点

            // 输出对应字符

            for (int j = 0; j < n; ++j) {

                if (mappings[j].index == p) {

                    printf("%c", mappings[j].ch);

                    break;

                }

            }

            p = m - 1; // 返回根节点

        }

    }

    printf("\n");

}


int main() {

    int n, i;

    printf("**********************编码译码系统************************\n");

    printf("--------开始构建哈夫曼树及哈夫曼编码-------\n");

    printf("1.请输入需编码的字符数目n：\n");

    scanf("%d", &n); fflush(stdin);

    //清空缓冲区，不清空缓冲区的话上一个字符还在缓冲区内，就产生错误了

    char* c = (char*)malloc(n * sizeof(char));    //存字符

    int* w = (int*)malloc(n * sizeof(int));    //存权重

    printf("2.请输入字符和对应的权值:\n");

    for (i = 0; i < n; i++) {

        scanf("%c %d", &c[i], &w[i]);

        fflush(stdin);

    }

    HuffmanTree HT;

    HuffmanCode HC;

    CreateHuffmanTree(HT, w, n);

    printf("\n哈夫曼树创建成功！\n");

    HuffmanCoding(HT, HC, w, n);

    //输出哈夫曼树

    printf("哈夫曼树为：\n");

    printf("char weight parent lchild rchild\n");

    for (i = 0; i < n; ++i) {

        printf("%4c %5d %6d %6d %6d\n", c[i], w[i], HT[i].parent, HT[i].lchild, HT[i].rchild);

    }

    //各字符编码

    printf("\n各字符对应的编码为：\n");

    for (i = 0; i < n; ++i) {

        printf("%c %s\n", c[i], HC[i]);

    }

    //报文编码

    int n1;

    printf("\n");

    printf("----------------哈夫曼树构造成功！开始编码译码功能-----------------\n");

    printf("3.编码功能\n\n请输入需要编译的字符总数(包括空格)：\n");

    scanf("%d", &n1);

    printf("\n");

    printf("请输入需编译的句子：\n");

    char* cc = (char*)malloc(n1 * sizeof(char));

    int m;

    for (m = 0; m <= n1; m++) {

        scanf("%c", &cc[m]);

    }

    printf("此句话编码结果为：\n");

    for (m = 0; m <= n1; m++) {

        for (int j = 0; j < 27; j++) {

            if (cc[m] == c[j]) {

                printf("%s", HC[j]);

            }

        }

    }

    printf("\n");



    //报文译码解码功能

    // 在main函数中，创建映射数组

    CharIndexMapping* mappings = (CharIndexMapping*)malloc(n * sizeof(CharIndexMapping));

    for (i = 0; i < n; i++) {

        mappings[i].ch = c[i];

        mappings[i].index = i;

    }

    printf("\n");


    // 解码功能

    HuffmanDecoding(HT, mappings, n);

    free(c);

    free(w);

    free(HT); // 释放哈夫曼树内存

    for (i = 0; i < n; ++i) {

        free(HC[i]); // 释放哈夫曼编码表内存

    }

    free(HC);

    free(mappings); // 释放映射数组内存

    return 0;

}