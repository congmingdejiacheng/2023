#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
using namespace std;
#define OK 1
#define ERROR 0
#define OVERFLOW -2
#define MAXSIZE 50
typedef int Status;
typedef struct
{
	int* base;
	int* top;
	int stacksize;
}SqStack;//定义顺序栈，存储数据类型为 int 型
//初始化栈：构造一个空栈
Status InitStack(SqStack& S)
{
	S.base = new int[MAXSIZE];
	if (!S.base) exit(OVERFLOW);
	S.top = S.base;
	S.stacksize = MAXSIZE;
	return OK;
}
//销毁栈
Status DestroyStack(SqStack& S)
{
	if (S.base) delete S.base;
	else return ERROR;
	S.base = S.top = NULL;
	S.stacksize = 0;
	return OK;
}
//清空栈，重新进栈元素会把原栈元素覆盖
void ClearStack(SqStack& S)
{
	S.top = S.base;
}
//判断栈是否为空
bool StackEmpty(SqStack S)
{
	if (S.top == S.base) return true;
	else return false;
}
//求栈的长度
int StackLength(SqStack S)
{
	return (S.top - S.base);
}
//获取栈顶元素值
int GetTop(SqStack S)
{
	if (S.top != S.base)
		return *(S.top - 1);
}
//入栈,插入元素 e 为栈顶元素
Status Push(SqStack& S, int e)
{
	if (S.top - S.base == S.stacksize)
		return ERROR; 
	*S.top++ = e;
	return OK;
}
//出栈，栈顶元素出栈，用 e 返回其值
Status Pop(SqStack& S, int& e)
{
	if (S.top == S.base) return ERROR;
	e = *--S.top;
	return OK;
}
//遍历栈中元素并显示
Status StackTraverse(SqStack S)
{
	int len;
	if (S.base)
		len = StackLength(S);
	else return ERROR;
	cout << "从栈底到栈顶元素依次为：" << endl;
	if (len == 0)
		cout << "栈中暂无数据" << endl;
	else
		for (int i = 0; i < len; i++)
		{
			cout << S.base[i] << " ";
		}
	cout << endl;
	return OK;
}
//将十进制数 N，转换成 M 进制数
void conversion(int N, int M)
{
	SqStack s;
	InitStack(s); //初始化栈 s
	while (N)
	{
		Push(s, N % M); //余数入栈
		N = N / M;
	}
	while (!StackEmpty(s))
	{
		int e;
		Pop(s, e); //栈顶元素出栈存入 e 中
		switch (e)
		{
		case 10:cout << 'A'; break;
		case 11:cout << 'B'; break;
		case 12:cout << 'C'; break;
		case 13:cout << 'D'; break;
		case 14:cout << 'E'; break;
		case 15:cout << 'F'; break;
		default:cout << e; break;
		}
	}
	cout << endl;
	DestroyStack(s);
}
//括号匹配
bool Parenthesis(char*arr,int n)
{
	SqStack s;
	InitStack(s); //初始化栈 s
    int i = 0;
	
	while (i < n)
	{
		if (arr[i] == '(' || arr[i] == '[' || arr[i] == '{')
		{
			Push(s, arr[i]);
			i++;
		}
		else if (arr[i] == ')' || arr[i] == ']' || arr[i] == '}')
		{
			
			//右括号多
			if (StackEmpty(s))
			{
				return false;
			}
			int e;
			Pop(s, e);
			char e1 = (char)e;
			//匹配错误
			if (arr[i] == ')' || e1 == '(')
			{
				i++;
				continue;
				
			}
			else if (arr[i] == ']' || e1 == '[')
			{
				i++;
				continue;
				
			}
			else if (arr[i] == '}' || e1 == '{')
			{
				i++;
				continue;
			}
			else
			{
				return false;
			}
			

		}
		else
		{
			return false;
		}
	}
		//左括号多
		return StackEmpty(s);
		
	

}
int priority(char a)
{
	if (a == '*' || a == '/')
		return 2;
	else if (a == '+' || a == '-')
		return 0;
	else if (a == '(')
		return -1;
}
char* inToPo(char * inExpression,int n)
{
	//inExpression是中缀表达式
   //readNum（0，i）函数会将字符串0从i往后的完整的一个数字string返回
   //字符串0表示的是中缀表达式
   //且调用该函数会将i值变为数字最后一位的位置
   //h存放运算符号
	SqStack h;
	InitStack(h);
	char poExpression[1000];
	int a = 0;
	int  e;
	for (int i = 0; i < n; i++)
	{
		//判断是否是数字
		if (isdigit(inExpression[i]))
		{
			poExpression[a] = inExpression[i];
			a++;
		}
		else if (inExpression[i] == '(')
		{
			Push(h, inExpression[i]);
		}
		else if (inExpression[i] == ')')
		{
			//这一步骤会将中缀表达式的所有括号去掉
			while (GetTop(h) != '(')
			{
				poExpression[a]=GetTop(h);
				a++;
				Pop(h,e);
			}
			Pop(h,e);
		}
		else if (inExpression[i] == '+' || inExpression[i] == '-' || inExpression[i] == '*' || inExpression[i] == '/')
		{
			//优先级大进栈
			while (!StackEmpty(h) && priority(inExpression[i]) <= priority(GetTop(h)))
			{
				poExpression[a]= GetTop(h);
				a++;
				Pop(h, e);
			}
			Push(h,inExpression[i]);
		
		}

		

	}
	while (!StackEmpty(h))
	{
		poExpression[a] = GetTop(h);
		a++;
		Pop(h, e);
	}

	return poExpression;


}
bool isNumber(char token) {
	return  ('0' <= token && token <= '9');
}
//表达式计算
int ExEvaluation(char* inExpression, int m,int a)
{
	char* poExpression;
	poExpression=inToPo(inExpression, m);
	int n = m;
	int stk[100], top = 0;
	for (int i = 0; i < a; i++) {
	
		if (isNumber(poExpression[i])) {
			stk[top++] = poExpression[i]-'0';
		}
		else {
			int num2 = stk[--top];
			int num1 = stk[--top];
			switch (poExpression[i]) {
			case '+':
				stk[top++] = num1 + num2;
				break;
			case '-':
				stk[top++] = num1 - num2;
				break;
			case '*':
				stk[top++] = num1 * num2;
				break;
			case '/':
				stk[top++] = num1 / num2;
				break;
			}
		}
	}
	return stk[top - 1];

}

void menu()
{
	cout << endl;
	cout << "---------顺序栈的基本操作和应用---------" << endl;

	cout << "1.遍历显示栈中元素" << endl;
	cout << "2.入栈操作" << endl;
	cout << "3.出栈操作" << endl;
	cout << "4.求栈的长度" << endl;
	cout << "5.判断栈是否为空" << endl;
	cout << "6.获取栈顶元素" << endl;
	cout << "7.栈应用——进制转换" << endl;
	cout << "8.栈应用——括号匹配" << endl;
	cout << "9.栈应用——表达式的计算" << endl;
	cout << "10.退出" << endl;
	cout << "----------------------------------------" << endl;
	cout << endl;
}

int main()
{
	menu();
	SqStack s;
	InitStack(s);//初始化栈 s
	int opnum;//存储操作序号
	bool flag = true;
	while (flag)
	{
		cout << "请输入您要进行的操作：";
		cin >> opnum;
		switch (opnum)
		{
	
			
		case 1://遍历并显示栈中元素
			StackTraverse(s);
			break;
		case 2://入栈
		{
			int n;
			char f1 = 'y';
			while (f1 == 'y')
			{
				cout << "请输入入栈的元素值：";
				cin >> n;
				if (!Push(s, n))
				{
					cout << "栈满！" << endl;
					break;
				}
				cout << "继续入栈吗？y or n :";
				cin >> f1;
			}
			break;
		}
		case 3://出栈
		{
			int x;
			char f2 = 'y';
			while (f2 == 'y')
			{
				if (!Pop(s, x))
				{
					cout << "栈空！" << endl;
					break;
				}
				cout << "出栈的元素为" << x << endl;
				cout << "继续出栈吗？y or n:";
				cin >> f2;
			}
			break;
		}
		case 4://求栈长
			cout << "栈的长度为：" << StackLength(s) << endl;
			break;
		case 5://判断栈是否为空
			if (StackEmpty(s)) cout << "栈空" << endl;
			else cout << "栈不为空" << endl;
			break;
		case 6://获取栈顶元素
			cout << "栈顶元素为：" << GetTop(s) << endl;
			break;
		case 7://进制转换
		{
			int n, m;
			cout << "请输入一个非负十进制数：";
			cin >> n;
			cout << "请输入要转换成的进制（2,8,16）：";
			cin >> m;
			conversion(n, m);
			break;
		}
		case 8://括号匹配
		{
			cout << "请输入括号和一共的括号个数" << endl;
			char arr[1000] = { 0 };
			int n;
			cin >> arr>>n;
			bool b = Parenthesis(arr,n);
			if (b)
			{
				cout << "符号匹配" << endl;
			}
			else
			{
				cout << "符号不匹配" << endl;
			}
			break;
		}
		case 9://表达式计算
		{
			cout << "请输入表达式和一共表达式的个数和去掉（）的个数" << endl;
			char arr[1000] = { 0 };
			int n;
			int a;
			cin >> arr >> n>>a;
			int a1 = ExEvaluation(arr, n,a);
			cout << "表达式的答案是" << a1 << endl;
		}
			
		case 10://退出
			flag = false;
			DestroyStack(s); //销毁栈 s
			break;
		}
	}
	return 0;
}