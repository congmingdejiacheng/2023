#define _CRT_SECURE_NO_WARNINGS 1
//首先定义节点和边的类型然后定义最大值和有无向图
#include<iostream>
using namespace std;
template<class W, bool Direction = false>
struct Edge
{
	int  _dsti;
	W _w;
	Edge* next;
	Edge(const int dsti, W w)
	{
		_dsti = dsti;
		_w = w;
		next = nullptr;
	}
};
typedef Edge E;
int main()
{
	E<int> a(1, 2);
	return 0;
}