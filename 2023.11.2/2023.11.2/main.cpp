#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
using namespace std;
#define OK 1
#define ERROR 0
#define OVERFLOW -2
#define MAXQSIZE 100
typedef int Status;
typedef struct
{
	int num;
	char name[20];
}Person;//患者信息，包含病历号 num 和姓名 name
typedef struct
{
    Person* base;//定义存储基地址的指针 base
	int front;
	int rear;
}SqQueue;//顺序队列：存储患者的信息
//初始化一个空队列
Status InitQueue(SqQueue& Q)
{
	Q.base=new Person[MAXQSIZE];
	if (!Q.base) exit(OVERFLOW);
	Q.front=0;
	Q.rear = 0;
	return OK;
}
//获取队列长度
int QueueLength(SqQueue Q)
{
	return Q.rear - Q.front;
}
//队列为空则返回 true
bool IsQEmpty(SqQueue Q)
{
	if (Q.front == Q.rear)
		return true;
	else
		return false;
}
//队列满则返回 true
bool IsQFull(SqQueue Q)
{
	return (Q.rear + 1) % (MAXQSIZE) == Q.front;
}
//入队，从队尾(rear)入
Status EnQueue(SqQueue& Q, Person e)
{
	if (IsQFull(Q)) return ERROR;
	Q.base[Q.rear]=e;
	Q.rear++;
	Q.rear %= MAXQSIZE;
	return OK;
}
//出队，从队头(front)出
Status DeQueue(SqQueue& Q, Person& e)
{
	if (IsQEmpty(Q)) return ERROR;
	e = Q.base[Q.front];
	Q.front++;
	Q.front %= MAXQSIZE;

	return OK;
}
//获取队头元素
bool GetHead(SqQueue Q, Person& e)
{
	if (!IsQEmpty(Q))
	{
		e = Q.base[Q.front];
		return OK;
	}
	return ERROR;
}
//输出队列所有元素
void PrintQueue(SqQueue Q)
{
	int f = Q.front;
	int r = Q.rear;
	while (f<r)
	{
		cout << Q.base[f].num << " " << Q.base[f].name << endl;
		f++;
	}
	cout << endl;
}
//销毁队列
void DestroyQueue(SqQueue& Q)
{
	if (Q.base) delete[] Q.base;
}
//菜单
void menu()
{
	cout << endl;
	cout << "----------患者就诊管理系统------------" << endl;
	cout << "1.患者加入队列" << endl;
	cout << "2.叫号就诊" << endl;
	cout << "3.输出队列长度" << endl;
	cout << "4.查看所有排队人信息" << endl;
	cout << "5.退出系统" << endl;
	cout << "---------------------------------------" << endl;
	cout << endl;
}
//循环队列应用
int main()
{
	menu();
	SqQueue q;
	InitQueue(q);//初始化队列
	int opnum;
	bool flag = true;
	while (flag)
	{
		cout << "请输入您要进行的操作：";
		cin >> opnum;
		switch (opnum)
		{
		case 1://入队
		{
			Person p1;
			char c = 'y';
			while (c == 'y')
			{
				cout << "请输入病人病历号：" << endl;
				cin >> p1.num;
				cout << "请输入病人姓名：" << endl;
				cin >> p1.name;
				EnQueue(q,p1);
				cout << "继续录入吗？y or n：";
				cin >> c;
			}
			break;
		}
		case 2://出队
		{
			Person p2;
			DeQueue(q, p2);
			cout << "请" << p2.num << " " << p2.name << "就诊" << endl;
			DeQueue(q, p2);
			cout << "下一位：" << p2.num << " " << p2.name << "做准备" << endl;
			break;
		}
		case 3://求队长
			cout << "目前排队人数为：" << QueueLength(q) << endl;
			break;
		case 4://显示排队人的信息
			cout << "目前所有需就诊人员的信息为：" << endl;
			PrintQueue(q);
			break;
		case 5:
			flag = false;
			DestroyQueue(q); //销毁队列
			break;
		}
	}
	return 0;
}