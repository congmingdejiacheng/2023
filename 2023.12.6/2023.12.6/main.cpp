#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<vector>
using namespace std;

void moveZeroes(vector<int>& nums) {
    int dest = 0;
    int cur = 0;
    if (nums[0] == 0)
    {
        dest = -1;
    }
    while(cur < nums.size())
    {
        if (nums[cur] != 0)
        {
            dest++;
            swap(nums[cur], nums[dest]);
        }
        else
        {
            cur++;
        }

    }
    for (auto v : nums)
    {
        cout << v <<" ";
    }

}
int main()
{
    vector<int> v;
    int arr[10] = { 0,1,0,3,12 };
    for (int i = 0; i < sizeof(arr) / 4; i++)
    {
        v.push_back(arr[i]);
    }
    moveZeroes(v);
    return 0;
}