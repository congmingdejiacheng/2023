#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include<conio.h>
#include<string.h>
#include<windows.h>
#include <malloc.h>
#include<dos.h>
#include<assert.h> 
using namespace std;
#define len sizeof(struct dorminfo)
#define Max 100 
int a[Max] = { 0 };
int length;
void showmenu();
void processmenu();
void create();
void display();
void disbed();
void disname();
void disstu();
void modify();
void del();
void BubbleSort();
void BubbleSort1(struct dorminfo* L);
void Quicksort1(struct dorminfo* head, struct dorminfo* end);
struct dorminfo* GetPartion(struct dorminfo* pBegin, struct dorminfo* pEnd);
void quicksort();
typedef struct
{
	int dormno;
	int bedno;
	int isstay;
}bedinfo;
struct dorminfo
{
	bedinfo bed;
	int stuno;
	char name[20];
	char stucla[20];
	int year;
	int month;
	struct dorminfo* next;
};
struct dorminfo* head = NULL, * p1, * p2, * p3;

void showmenu()
{
	printf("\n\n\n\t\t 欢迎进入学生宿舍管理系统\n");
	printf("\t\t*********************************\n");
	printf("\t\t1.输入床位信息\n");
	printf("\t\t2.根据学号,显示学生住宿信息\n");
	printf("\t\t3.根据宿舍号,显示学生住宿信息\n");
	printf("\t\t4.根据姓名,显示学生住宿信息\n");
	printf("\t\t5.根据宿舍号、床位号,修改住宿信息\n");
	printf("\t\t6.根据宿舍号、床位号,删除住宿信息\n");
	printf("\t\t7.根据宿舍号进行冒泡遍历 \n");
	printf("\t\t8.根据学号进行快速遍历 \n");
	printf("\t\t9.退出 \n");
	printf("\t\t********************************\n");
}
void processmenu()
{
	int caidan;
	printf("请输入您的选项<1~8>:");
	cin >> caidan;
	if (caidan > 9 || caidan < 0)
	{
		printf("对不起，您输入的选项有误，请重新输入!");
		cin >> caidan;
	}
	switch (caidan)
	{
	case 1:create(); break;
	case 2:display(); break;
	case 3:disbed(); break;

	case 4:disname(); break;
	case 5:modify(); break;
	case 6:del();  break;
	case 7:BubbleSort(); break;
	case 8:quicksort(); break;
	case 9:exit(0);
	}
}
void BubbleSort()
{
	
	BubbleSort1(head);
	return;

}
void BubbleSort1(struct dorminfo* L)
{
	int i, count = 0, num;//count记录链表结点的个数，num进行内层循环，
	struct dorminfo* p, * q, * tail;//创建三个指针，进行冒泡排序
	p = L;
	while (p->next != NULL)//计算出结点的个数
	{
		count++;//注释①
		p = p->next;
	}
	for (i = 0; i < count - 1; i++)//外层循环，跟数组冒泡排序一样
	{
		num = count - i - 1;//记录内层循环需要的次数，跟数组冒泡排序一样，
		q = L->next;//令q指向第一个结点
		p = q->next;//令p指向后一个结点
		tail = L;//让tail始终指向q前一个结点，方便交换，也方便与进行下一步操作
		while (num--)//内层循环 次数跟数组冒泡排序一样
		{
			if (q->bed.bedno > p->bed.bedno)//如果该结点的值大于后一个结点，则交换
			{
				q->next = p->next;
				p->next = q;
				tail->next = p;
			}
			tail = tail->next;//注释②
			q = tail->next;//注释②
			p = q->next;//注释②
		}
	}
	p3 = head->next;
	
	while (p3 != NULL)
	{
		
			printf("\n住宿信息如下:");
			printf("  宿舍号:%d", p3->bed.dormno);
			printf("  床位号:%d", p3->bed.bedno);
			printf("  是否有人入住:%d", p3->bed.isstay);
			printf("  学生学号:%d", p3->stuno);
			printf("  学生姓名:%s", p3->name);
			printf("  学生班级:%s", p3->stucla);
			printf("  入住时间为:%d年%d月\n", p3->year, p3->month);
	
		p3 = p3->next;
	}
}
void quicksort()
{
	int count=0;
	struct dorminfo* p=head;
	while (p->next != NULL)//计算出结点的个数
	{
		count++;//注释①
		p = p->next;
	}
	Quicksort1(head->next, NULL);
	p3 = head->next;

	while (p3 != NULL)
	{

		printf("\n住宿信息如下:");
		printf("  宿舍号:%d", p3->bed.dormno);
		printf("  床位号:%d", p3->bed.bedno);
		printf("  是否有人入住:%d", p3->bed.isstay);
		printf("  学生学号:%d", p3->stuno);
		printf("  学生姓名:%s", p3->name);
		printf("  学生班级:%s", p3->stucla);
		printf("  入住时间为:%d年%d月\n", p3->year, p3->month);

		p3 = p3->next;
	}
}

struct dorminfo* GetPartion(struct dorminfo* pBegin, struct dorminfo* pEnd)
{
	int key = pBegin->stuno;
	struct dorminfo* p = pBegin;
	struct dorminfo* q = p->next;

	while (q != pEnd)
	{
		if (q->stuno < key)
		{
			p = p->next;
			swap(p->stuno, q->stuno);
			swap(p->bed.bedno, q->bed.bedno);
			swap(p->bed.dormno, q->bed.dormno);
			swap(p->bed.isstay, q->bed.isstay);
			swap(p->month, q->month);
			swap(p->name, q->name);
			swap(p->stucla, q->stucla);
			swap(p->year, q->year);
		

		}

		q = q->next;
	}
	swap(p->stuno, pBegin->stuno);
	swap(p->bed.bedno, pBegin->bed.bedno);
	swap(p->bed.dormno, pBegin->bed.dormno);
	swap(p->bed.isstay, pBegin->bed.isstay);
	swap(p->month, pBegin->month);
	swap(p->name, pBegin->name);
	swap(p->stucla, pBegin->stucla);
	swap(p->year, pBegin->year);
	return p;
}
void Quicksort1(struct dorminfo* head, struct dorminfo* end) {
	if (head != end)
	{
		struct dorminfo* partion = GetPartion(head, end);
		Quicksort1(head, partion);
		Quicksort1(partion->next, end);
	}

}
void create()
{
	int j;
	char ch;
	length = 0;
	p1 = (struct dorminfo*)malloc(len);
	if (p1 == NULL)
	{
		assert(false);
	}
	if (head == NULL)
		head = p1;
	printf("开始输入床位信息....\n");
	Sleep(500);
	do
	{
		p2 = (struct dorminfo*)malloc(len);
		if (p2 == NULL)
		{
			assert(false);
		}
		printf("请输入宿舍号:");
		cin >> p2->bed.dormno;
		printf("请输入床号:");
		scanf_s("%d", &p2->bed.bedno);
		printf("是否有人居住(1/0):");
		scanf_s("%d", &p2->bed.isstay);
		printf("请输入学生学号:");
		scanf_s("%d", &p2->stuno);
		a[length] = p2->stuno;
		if (length > 1)
		{
			for (j = 1; j < length; j++)
			{
				if (a[length] == a[j])
				{
					printf("该床位号已存在，请重新输入:");
					scanf_s("%d", &p2->bed.bedno);
				}
			}
		}
		printf("请输入学生姓名:");
		cin >> p2->name;
		printf("请输入学生班级:");
		cin >> p2->stucla;
		printf("请输入学生入住时间(年 月)<如2015 5>:");
		scanf_s("%d%d", &p2->year, &p2->month);
		if (p2->year < 1000 || p2->year>9999 || p2->month > 12 || p2->month < 1)
		{
			printf("对不起，输入错误，请重新输入!\n");
			scanf_s("%d%d\n", &p2->year, &p2->month);
		}
		length++;
		p1->next = p2;
		p2->next = NULL;
		p1 = p1->next;
		printf("第%d个住宿信息创建成功!\n", length);
		Sleep(300);
		fflush(stdin);
		printf("是否继续添加住宿信息?<y/Y>");
		cin >> ch;
	} while (ch == 'y' || ch == 'Y');
}
void display()
{
	int flag = 0, No;
	p3 = head->next;
	printf("请输入学号:");
	scanf_s("%d", &No);
	while (p3 != NULL)
	{
		if (p3->stuno == No)
		{
			printf("\n住宿信息如下:");
			printf("\n宿舍号:%d", p3->bed.dormno);
			printf("\n床位号:%d", p3->bed.bedno);
			printf("\n是否有人入住:%d", p3->bed.isstay);
			printf("\n学生学号:%d", p3->stuno);
			printf("\n学生姓名:%s", p3->name);
			printf("\n学生班级:%s", p3->stucla);
			printf("\n入住时间为:%d年%d月\n", p3->year, p3->month);
			flag = 1;
		}
		p3 = p3->next;
	}
	if (!flag)
		printf("没有找到该学生住宿信息！\n");
}
void disname()
{
	int flag = 0;
	string DormNo;
	p3 = head->next;
	printf("请输入姓名:");
	cin >> DormNo;
	while (p3 != NULL)
	{
		if (DormNo.compare(p3->name)==0)
		{
			printf("\n住宿信息如下:");
			printf("\n宿舍号:%d", p3->bed.dormno);
			printf("\n床位号:%d", p3->bed.bedno);
			printf("\n是否有人入住:%d", p3->bed.isstay);
			printf("\n学生学号:%d", p3->stuno);
			printf("\n学生姓名:%s", p3->name);
			printf("\n学生班级:%s", p3->stucla);
			printf("\n入住时间为:%d年%d月\n", p3->year, p3->month);
			flag = 1;
		}
		p3 = p3->next;
	}
	if (!flag)
		printf("没有找到该学生住宿信息！\n");
}
void disstu()
{
	int flag = 0, DormNo;
	p3 = head->next;
	printf("请输入学号:");
	cin >> DormNo;
	while (p3 != NULL)
	{
		if (p3->stuno == DormNo)
		{
			printf("\n住宿信息如下:");
			printf("\n宿舍号:%d", p3->bed.dormno);
			printf("\n床位号:%d", p3->bed.bedno);
			printf("\n是否有人入住:%d", p3->bed.isstay);
			printf("\n学生学号:%d", p3->stuno);
			printf("\n学生姓名:%s", p3->name);
			printf("\n学生班级:%s", p3->stucla);
			printf("\n入住时间为:%d年%d月\n", p3->year, p3->month);
			flag = 1;
		}
		p3 = p3->next;
	}
	if (!flag)
		printf("没有找到该学生住宿信息！\n");
}
void disbed()
{
	int flag = 0, DormNo;
	p3 = head->next;
	printf("请输入宿舍号:");
	cin >> DormNo;
	while (p3 != NULL)
	{
		if (p3->bed.dormno == DormNo)
		{
			printf("\n住宿信息如下:");
			printf("\n宿舍号:%d", p3->bed.dormno);
			printf("\n床位号:%d", p3->bed.bedno);
			printf("\n是否有人入住:%d", p3->bed.isstay);
			printf("\n学生学号:%d", p3->stuno);
			printf("\n学生姓名:%s", p3->name);
			printf("\n学生班级:%s", p3->stucla);
			printf("\n入住时间为:%d年%d月\n", p3->year, p3->month);
			flag = 1;
		}
		p3 = p3->next;
	}
	if (!flag)
		printf("没有找到该学生住宿信息！\n");
}
void modify()
{
	struct dorminfo* p = NULL;
	int DormNo, BedNo;
	int flag = 0;
	int Dormno, Bedno, Isstay, Stuno, Year, Month;
	char Name[20], Stucla[20];
	if (head == NULL) {
		printf("住宿信息为空,不能修改,按任意键返回...\n");

		system("cls");
		return;
	}
	p1 = p3 = head;
	printf("请输入宿舍号、床位号:");
	cin >> DormNo >> BedNo;
	while (p3 != NULL)
	{
		if (p3->bed.dormno == DormNo && p3->bed.bedno == BedNo)
		{
			printf("已找到要修改的宿舍号、床位号!\n");
			flag = 1;
			p = p3;
			Sleep(1000);
		}
		p3 = p3->next;
	}
	if (flag)
	{
		printf("请输入修改后的宿舍号:");
		scanf_s("%d", &Dormno);
		printf("请输入修改后的床号:");
		scanf_s("%d", &Bedno);
		printf("是否有人居住(1/0):");
		scanf_s("%d", &Isstay);
		printf("请输入修改后的学生学号:");
		scanf_s("%d", &Stuno);
		while (p1 != NULL)
		{
			if (p1->stuno == Stuno) {
				printf("该床位号已存在，请重新输入:");
				scanf_s("%d", &Stuno);
			}
			p1 = p1->next;
		}
		printf("请输入修改后的学生姓名:");
		cin >> Name;
		printf("请输入修改后的学生班级:");
		cin >> Stucla;
		printf("请输入修改后的学生入住时间(年月)<如2015 5>:");
		scanf_s("%d%d", &Year, &Month);
		if (Year < 1000 || Year>9999 || Month > 12 || Month < 1)
		{
			printf("对不起,输入错误,请重新输入!\n");
			scanf_s("%d%d", &Year, &Month);
		}
		p->bed.dormno = Dormno;
		p->bed.bedno = Bedno;
		p->bed.isstay = Isstay;
		p->stuno = Stuno;
		strcpy_s(p->name, Name);
		strcpy_s(p->stucla, Stucla);
		p->year = Year;
		p->month = Month;
		printf("修改成功,信息如下:\n");
		printf("-宿舍号--床号--有无人入住--学号--姓名--班级--入住时间(年月)");
		printf("\n");
		printf("%6d%5d%8d%9d%7s%8s%9d%2d\n", p->bed.dormno, p->bed.bedno, p->bed.isstay, p->stuno, p->name, p->stucla, p->year, p->month);
	}
	else
	{
		printf("没有找到该宿舍号与床号信息,按任意键返回...\n");

		system("cls");
		return;
	}
}
void del()
{
	int DormNo, BedNo;
	p1 = head, p2 = head->next;
	printf("请输入宿舍号、床位号:");
	scanf_s("%d %d", &DormNo, &BedNo);
	while (p2 != NULL)
	{
		if (p2->bed.dormno == DormNo && p2->bed.bedno == BedNo)
		{
			p1->next = p2->next;
			free(p2);
			length--;
			printf("删除成功!\n");
			system("cls");
			return;
		}
		p2 = p2->next;
		p1 = p1->next;
	}
	if (p1 == NULL)
		printf("找不到要删除的宿舍号、床位号!\n");
}




int main()
{
	while (1)
	{
		showmenu();
		processmenu();
		system("pause");
		system("cls");
	}
}

