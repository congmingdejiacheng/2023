#include<iostream>
#include<math.h>
using namespace std;
int main()
{
    int a, b, c;
    cin >> a >> b >> c;
    double p = (a + b + c) / 2;
    double s = sqrt(p * (p - a) * (p - b) * (p - c));
    printf("%.1lf\n", s);
    return 0;
}