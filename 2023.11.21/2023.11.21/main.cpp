#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<set>
using namespace std;
void test_set1()
{
	set<int> s;
	s.insert(5);
	s.insert(2);
	s.insert(6);
	s.insert(1);
	s.insert(1);
	pair<set<int>::iterator, bool>ret2 = s.insert(8);
	cout << ret2.second << endl;
	for (auto e : s)
	{
		cout << e << " ";
	}
	cout << endl;
	////方法一
	set<int>::iterator it = s.find(3);
	s.erase(it);
	for (auto e : s)
	{
		cout << e << " ";
	}
	if (s.count(3))
	{
		cout << "在" << endl;
	}
	////方法二
	//s.erase(7);
	//for (auto e : s)
	//{
	//	cout << e << " ";
	//}
}
void test_set2()
{
	std::set<int>myset;
	std::set<int>::iterator itlow, itup;
	for (int i = 1; i < 10; i++)
	{
		myset.insert(i * 10);
	}
	itlow = myset.lower_bound(30);//  >=val值的位置这里返回的是30
	itlow = myset.lower_bound(25);
	itup = myset.upper_bound(70);//   >val值位置这里返回的是80
	myset.erase(itlow, itup);//这里删除的是30到70；
	for (auto e : myset)
	{
		cout << e << " ";
	}

 }
void test_set3()
{
	std::set<int> myset;

	for (int i = 1; i <= 5; i++) myset.insert(i * 10);   // myset: 10 20 30 40 50

	std::pair<std::set<int>::const_iterator, std::set<int>::const_iterator> ret;
	ret = myset.equal_range(30);

	std::cout << "the lower bound points to: " << *ret.first << '\n'; //first是>=val的值————30
	std::cout << "the upper bound points to: " << *ret.second << '\n';//second是>val的值————40

	
}
int main()
{
	test_set3();

	return 0;
}