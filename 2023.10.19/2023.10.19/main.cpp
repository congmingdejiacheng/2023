#define _CRT_SECURE_NO_WARNINGS 1

//ADD.h
int Add(int x1, int x2);



//ADD.cpp
int Add(int x1, int x2)
{
    return x1 + x2;
}


//main.cpp
int main()
{
    cout << Add(1, 2) << endl;
    return 0;
}
