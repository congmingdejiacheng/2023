#define _CRT_SECURE_NO_WARNINGS 1
```C+ + []
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
    class Solution {
    public:
        vector<vector<int>> levelOrderBottom(TreeNode* root) {
            queue <TreeNode*> q;
            int  levelSize = 0;

            if (root)
            {
                q.push(root);
                levelSize = 1;
            }
            vector <vector<int>> vv;
            while (!q.empty())
            {
                vector <int> v;
                while (levelSize--)
                {

                    //һ�������;
                    TreeNode* front = q.front();
                    q.pop();
                    v.push_back(front->val);
                    if (front->left)
                        q.push(front->left);
                    if (front->right)
                        q.push(front->right);
                }

                vv.push_back(v);
                levelSize = q.size();
            }
            reverse(vv.begin(), vv.end());
            return vv;
        }
};
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    vector<vector<int>> levelOrder(TreeNode* root) {
        queue<TreeNode*> q;
        vector<vector<int>> vv;
        int line = 0;
        if (root)
        {
            q.push(root);
            line = 1;
        }
        while (!q.empty())
        {
            vector<int> v;
            while (line--)
            {

                TreeNode* s = q.front();
                q.pop();
                v.push_back(s->val);
                if (s->left)
                {
                    q.push(s->left);
                }
                if (s->right)
                {
                    q.push(s->right);
                }
            }
            vv.push_back(v);
            line = q.size();
        }

        return vv;
    }
};