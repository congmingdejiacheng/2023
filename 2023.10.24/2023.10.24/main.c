#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
void InsertSort(int* a, int n)
{
	for (int i = 0; i < n-1; i++)
	{
		int end = i;
		int tmp = a[i+1];
		while (end > 0)
		{
			if (a[end] > tmp)
			{
				a[end + 1] = a[end];
				end--;
			}
			else
			{
				break;
			}
		}
		a[end + 1] = tmp;
		
	}
}
void PrintfArray(int* a, int n)
{
	for (int i = 0; i < n; ++i)
	{
		printf("%d ", a[i]);
	}
	printf("\n");
}
//void ShellSort(int* a,int n)
//{
//	int gap = 3;
//	for (int j = 0; j < gap; j++)
//	{
//		for (int i = j; i < n-gap; i += gap)
//		{
//			int end = i;
//			int tmp = a[i + gap];
//			while (end >= 0)
//			{
//				if (a[end] > tmp)
//				{
//					a[end + gap] = a[end];
//					end -= gap;
//				}
//				else
//				{
//					break;
//				}
//			}
//			a[end+gap] = tmp;
//
//		}
//	}
//	
//}
void ShellSort(int* a, int n)
{
	int gap = n;
	while (gap > 1)
	{
		gap = gap / 3 + 1;
		//gap = gap / 2;
		for (int i = 0; i < n - gap; i++)
		{
			int end = i;
			int tmp = a[i + gap];
			while (end >= 0)
			{
				if (a[end] > tmp)
				{
					a[end + gap] = a[end];
					end -= gap;
				}
				else
				{
					break;
				}
			}
			a[end + gap] = tmp;

		}
	}

}
void Swap(int* p1, int* p2)
{
	int a = *p1;
	*p1 = *p2;
	*p2 = a;

}
void SelectSort(int* a, int n)
{
	int begin = 0, end = n - 1;
	while (begin < end)
	{
		int max = begin, min = begin;
		for (int i = begin; i <= end; i++)
		{
			if (a[max] < a[i])
			{
				max = i;
			}
			if (a[min] > a[i])
			{
				min = i;
			}
		}
		Swap(&a[min], &a[begin]);
		if (max == begin)
		{
			max = min;
		}
		Swap(&a[max], &a[end]);
		
		
		begin++;
		end--;
	}
	
 }
int main()
{
	int arry[10] = { 9,8,7,6,5,4,3,2,1,0 };
	PrintfArray(arry, 10);
	SelectSort(arry, 10);
	PrintfArray(arry, 10);
	return 0;
}