#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
using namespace std;
#include"reverse_iterator.h"
#include "vector.h"

int main()
{
	bit::vector<int> lt;
	lt.push_back(1);
	lt.push_back(2);
	lt.push_back(3);
	lt.push_back(4);
	lt.push_back(5);

	bit::vector<int>::iterator it = lt.begin();
	while (it != lt.end())
	{
		cout << *it << " ";
		++it;
	}
	cout << endl;

	bit::vector<int>::reverse_iterator rit = lt.rbegin();
	while (rit != lt.rend())
	{
		//*rit += 1;

		cout << *rit << " ";
		++rit;
	}
	cout << endl;

	return 0;
}