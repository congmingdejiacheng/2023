#pragma once
// 将以下代码适配到vector和list中做反向迭代器，理解反向迭代器的原理

	// 适配器 -- 复用
	//template<class Iterator, class Ref, class Ptr>
	//class ReverseIterator
	//{
	//public:
	//	typedef ReverseIterator<Iterator, Ref, Ptr>Self;
	//	ReverseIterator(Iterator it)
	//		:_it(it)
	//	{}
	//	Self& operator ++()
	//	{
	//		--_it;
	//		return *this;
	//	}

	//	Self& operator --()
	//	{
	//		++_it;
	//		return *this;
	//	}
	//	Ref opertor* ()
	//	{
	//		Iterator cur = _it;
	//		return *(--cur);
	//	}
	//	Ptr operator ->()
	//	{
	//		return &(operator *());
	//	}
	//	bool operator !=(const Self& s)
	//	{
	//		return _it != s._it;
	//	}
	//template<class Iterator, class Ref, class Ptr>
	//class ReverseIterator
	//{
	//public:
	//	typedef ReverseIterator<Iterator, Ref, Ptr>Self;
	//	ReverseIterator(Iterator it)
	//		:_it(it)
	//	{}
	//	Self& operator ++()
	//	{
	//		--_it;
	//		return *this;
	//	}

	//	Self& operator --()
	//	{
	//		++_it;
	//		return *this;
	//	}
	//

	//	Ref operator* ()
	//	{
	//		Iterator cur = _it;
	//		return *(--cur);
	//	}

	//	Ptr operator ->()
	//	{
	//		return &(operator *());
	//	}
	//	bool operator !=(const Self& s)
	//	{
	//		return _it != s._it;
	//	}
 //   private:
	//	Iterator _it;
	//};
////
////#pragma once
////
//// vector::iterator  -> ReverseIterator
//// list::iterator    -> ReverseIterator
// ...
//
//template<class Iterator, class Ref, class Ptr>
//class ReverseIterator
//{
//public:
//	typedef ReverseIterator<Iterator, Ref, Ptr> Self;
//
//	ReverseIterator(Iterator it)
//		:_it(it)
//	{}
//
//	Self& operator++()
//	{
//		--_it;
//		return *this;
//	}
//
//	Self& operator--()
//	{
//		++_it;
//		return *this;
//	}
//
//	Ref operator*()
//	{
//		Iterator cur = _it;
//		return *(--cur);
//	}
//
//	Ptr operator->()
//	{
//		return &(operator*());
//	}
//
//	bool operator!=(const Self& s)
//	{
//		return _it != s._it;
//	}
//
//	bool operator==(const Self& s)
//	{
//		return _it == s._it;
//	}
//private:
//	Iterator _it;
//};