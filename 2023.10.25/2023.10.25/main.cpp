#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<string>
using namespace std;
//class Person
//{
//public:
//	Person()
//	{
//
//	}
//	Person(const char* name, int num)	
//	{
//		_name = name;
//		_num = num;
//
//	}
//	Person(const Person &p1)//c++以后多使用&这里将参数的类型分为两种：输入型参数；输出型参数；这里输入型参数：在c++中多使用const+&；对于输出型参数：多使用&；
//	{
//		_name = p1._name;
//		_num = p1._num;
//	}
//	
//	void print()
//	{
//		cout << "姓名" << _name << "学号" << _num << endl;
//	}
//	Person& operator=(const Person& p2)
//	{
//		_name = p2._name;
//		_num = p2._num;
//		return *this;
//	}
//	~Person()
//	{
//		cout << "~Person()"<<endl;
//
//	}
//	friend void printf(const Person& p, const Student& s);
//	
//protected:
//	string _name = "小李子";
//		int _num = 111;
//
//};
//
//class Student : public Person
//{
//public:
//	Student()
//	{
//
//	}
//	Student(const char* name, int num, int l) :Person(name,num), _life(l)
//	{
//		cout << "Student(const char* name, int num, int l)" << endl;
//	}
//	Student(const Person & p1,int l):Person(p1),_life(l)
//	{
//		cout<<"Student(const Person & p1,int l)"<<endl;
//	}
//	void print()
//	{
//		cout << "姓名" << _name << endl;
//		cout << "学号" << _num << endl;
//		cout << "生活费" << _life << endl;
//	}
//	Student& operator =(const Student & s)
//
//	{
//		if (&s != this)
//		{
//			Person::operator=(s);//	this->Person::operator=(s);这里是调用省略this指针来调用父类的赋值函数；
//			_life = s._life;
//		}
//		return *this;
//
//	}
//	~Student()
//	{
//		cout << "~Strudent()" << endl;
//	}
//	friend void printf(const Person& p, const Student& s);
//
//protected: 
//	int _life = 2000;
//
//
//};
//void printf(const Person& p, const Student& s)
//{
//	cout << p._name << endl;
//	cout << s._name << endl;
//
//}
//////////////////////////////////////////////////////////////////////////////
//class Person
//{
//public:
//	friend void printf(const Person& p, const Student& s);
//protected:
//	string _name = "小李子";
//	int _num = 111;
//};
//class Student : public Person
//{
//public:	
//	friend void printf(const Person& p, const Student& s);
//protected:
//	int _life = 2000;
//};
//void printf(const Person& p, const Student& s)
//{
//	cout << p._name << endl;
//	cout << s._name << endl;
//
//}
//
//void text1()
//{
//	Person 
//
//}
//int main()
//{
//	text1();
//	return 0;
//}
//

//class Student; // 前向声明
//
//class Person
//{
//public:
//	friend void Display(const Person& p, const Student& s);
//
//protected:
//	string _name ;
//};
//class Student : public Person
//{
//	
//protected:
//	int _stuNum ;
//};
//void Display(const Person& p, const Student& s)
//{
//	cout << p._name << endl;
//	cout << s._stuNum << endl;
//}
//void main()
//{
//	Person p;
//	Student s;
//	Display(p, s);
//}
//#include <iostream>
//#include <string>
//using namespace std;
//
//class Student; // 前向声明
//
//class Person
//{
//public:
//	friend void Display(const Person& p, const Student& s);
//	Person(const string& name) : _name(name) {}
//protected:
//	string _name; // 姓名
//};
//
//class Student : public Person
//{
//public:
//	friend void Display(const Person& p, const Student& s);
//	Student(const string& name, int stuNum) : Person(name), _stuNum(stuNum) {}
//protected:
//	int _stuNum; // 学号
//};
//
//void Display(const Person& p, const Student& s)
//{
//	cout << p._name << endl;
//	cout << s._stuNum << endl;
//}
//
//int main()
//{
//	Person p("Tom");
//	Student s("Jerry", 12345);
//	Display(p, s);
//
//	return 0;
//}

