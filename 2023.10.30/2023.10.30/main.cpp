#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
using namespace std;
//class Person
//{
//public :
//	string _name;
//
//};
//class Student:virtual public Person 
//{
//protected:
//	int num;
//};
//class Teacher :virtual public Person 
//{
//protected:
//	int _id;
//};
////class Assistant :public Student, public Teacher
////{
////protected:
////	string _majorCourse;
////};
////class A
////{
////public :
////	void func()
////	{
////
////	}
////protected:
////	int _a;
////
////
////};
////class B : public A 
////{
////public :
////	void f()
////	{
////		func();
////	}
////protected:
////	int _b;
////
////};
////class C
////{
////public :
////	void func()
////	{
////
////	}
////protected:
////	int _c;
////};
////class D
////{
////public :
////	void f()
////	{
////		_c.func();
////		
////	}
////protected:
////	C _c;
////	int _d;
////};
//class A
//{
//public :
//	virtual void func()
//	{
//		cout << "A" << endl;
//	}
//protected:
//	int _a;
//
//
//};
//class B : public A 
//{
//public :
//	
//	virtual void func()
//	{
//		cout << "B" << endl;
//	}
//
//protected:
//	int _b;
//
//};
//void vim (A& c)
//
//{
//	c.func();
//}
class A
{
public:
	void func()
	{}
protected:
	int _a;
};

class B : public A
{
public:
	void f()
	{
		func();
		_a++;
	}
protected:
	int _b;
};

class Person {
public:
	virtual B* BuyTicket()
	{
		cout << "Person买票-全价" << endl;
		return nullptr;
	}

	virtual ~Person()
	{
		cout << "~Person()" << endl;
	}
};

class Student : public Person {
public:
	virtual A* BuyTicket()
	{
		cout << "Student买票-半价" << endl;
		return nullptr;
	}

	/*注意：在重写基类虚函数时，派生类的虚函数在不加virtual关键字时，虽然也可以构成重写(因为继承后基类的虚函数被继承下来了在派生类依旧保持虚函数属性),但是该种写法不是很规范，不建议这样使用*/
	/*void BuyTicket() { cout << "买票-半价" << endl; }*/

	~Student()
	{
		cout << "~Student()" << endl;
	}
};
void Func(Person& p)
{
	p.BuyTicket();
}

int main()
{
	Person a;
	Student b;
	Func(a);
	Func(b);
	return 0;
}