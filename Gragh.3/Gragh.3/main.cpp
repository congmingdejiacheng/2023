#define _CRT_SECURE_NO_WARNINGS 1
//代码偏长，功能一个一个地写，先完成 case 1 和 case 2 功能的相关代码；然后完成 case 3
//功能的相关代码；最后完成 case 4 功能相关代码。
#include<iostream>
using namespace std;
#define OK 1
#define ERROR 0
#define OVERFLOW -2
typedef int Status;
//图的邻接矩阵存储方式，书 P148
#define MAXINT 32767
#define MVNum 20
typedef char VerTexType;
typedef int ArcType;
typedef struct {
	VerTexType vexs[MVNum]; //顶点数组，字符型
	ArcType arcs[MVNum][MVNum]; //邻接矩阵
	int vexnum, arcnum; //图的当前顶点数和弧数
}AMGraph;
//在顶点数组中定位顶点 v 的位置
int LocateVex(AMGraph G, char v)
{
	int i;
	for (i = 0; i < G.vexnum; i++)
		if (v == G.vexs[i])
		{
			return i;
		}
	return -1;
}
//参考书 P149 算法 6.1 无向网的创建
void CreateUDN(AMGraph& G)
{
	int i, j, k;
	char v1, v2;
	int w;
	cout << "请输入顶点数:" << endl;
	cin >> G.vexnum;
	cout << "请输入边数:" << endl;
	cin >> G.arcnum;
	cout << "请输入" << G.vexnum << "个顶点：" << endl;
	for (i = 0; i < G.vexnum; i++)
		cin >> G.vexs[i];
	//初始化邻接矩阵
	for (i = 0; i < G.vexnum; i++)
		for (j = 0; j < G.vexnum; j++)
			G.arcs[i][j] = MAXINT;
	//按实际构造网的邻接矩阵
	for (k = 0; k < G.arcnum; k++)
	{
		cout << "输入第" << k + 1 << "条边，形如 ab，a、b 分别为边的顶点:" << endl;
		cin >> v1 >> v2;
		cout << "输入该边的权值：" << endl;
		cin >> w;
		i = LocateVex(G, v1);
		j = LocateVex(G, v2);
		G.arcs[i][j] = w;
		G.arcs[j][i] = w;
	}
}
//网 G 中顶点 v 的第一个邻接点，不存在时返回-1
int FirstAdjVex(AMGraph& G, int v)
{
	int i;
	for (i = 0; i < G.vexnum; i++)
		if (G.arcs[v][i] < MAXINT)
			return i;
	return -1;
}
//网 G 中顶点 v 在 w 之后的下一个邻接点，不存在时返回-1
int NextAdjVex(AMGraph G, int v, int w)
{
	int i;
	for (i = w + 1; i < G.vexnum; i++)
		if (G.arcs[v][i] < MAXINT)
			return i;
	return -1;
}
void  Link(AMGraph G, char v, char w)
{
	int i = LocateVex(G, v);
	int j = LocateVex(G, w);
	if (G.arcs[i][j] != MAXINT)
	{
		cout << "存在链接" << endl;
	}
	else
	{
		cout << "不存在链接" << endl;

	}

}
//深度优先遍历图 P156 算法 6.3 和 6.4
bool visited1[MVNum];
void DFS(AMGraph G, int v)
{
	visited1[v] = true;
	cout << G.vexs[v];
	int w;
	for (w = FirstAdjVex(G, v); w >= 0; w = NextAdjVex(G, v, w))
		if (!visited1[w]) DFS(G, w);
}
void DFSTraverse(AMGraph G)
{
	int i;
	for (i = 0; i < G.vexnum; i++) visited1[i] = false;
	for (i = 0; i < G.vexnum; i++)
		if (!visited1[i]) DFS(G, i);
}
//广度优先遍历,需使用队列技术
//队列的定义
typedef struct
{
	int* base;
	int front;
	int rear;
}SqQueue;
//初始化一个空队列
Status InitQueue(SqQueue& Q)
{
	Q.base = new int[MVNum];
	if (!Q.base) exit(OVERFLOW);
	Q.front = Q.rear = 0;
	return OK;
}
//队列为空则返回 true
bool IsQEmpty(SqQueue Q)
{
	return Q.rear == Q.front;
}
//队列满则返回 true
bool IsQFull(SqQueue Q)
{
	return (Q.rear + 1) % MVNum == Q.front;
}
//入队，从队尾(rear)入
Status EnQueue(SqQueue& Q, int e)
{
	if (IsQFull(Q)) return ERROR;
	Q.base[Q.rear] = e;
	Q.rear = (Q.rear + 1) % MVNum;
}
//出队，从队首(front)出
Status DeQueue(SqQueue& Q, int& e)
{
	if (IsQEmpty(Q)) return ERROR;
	e = Q.base[Q.front];
	Q.front = (Q.front + 1) % MVNum;
	return OK;
}
//广度优先遍历算法 P158 算法 6.7
bool visited2[MVNum];
void BFS(AMGraph G, int v)
{
	int u, w;
	cout << G.vexs[v]; visited2[v] = true;
	SqQueue Q;
	InitQueue(Q);
	EnQueue(Q, v); //注：之前这条语句丢了 o(╥﹏╥)o
	while (!IsQEmpty(Q))
	{
		DeQueue(Q, u);
		for (w = FirstAdjVex(G, u); w >= 0; w = NextAdjVex(G, u, w))
			if (!visited2[w])
			{
				visited2[w] = true;
				cout << G.vexs[w];
				EnQueue(Q, w);
			}
	}
}
void BFSTraverse(AMGraph G)
{
	int i;
	for (i = 0; i < G.vexnum; i++) visited2[i] = false;
	for (i = 0; i < G.vexnum; i++)
		if (!visited2[i]) BFS(G, i);
}
//最小生成树：Prim 算法
struct {
	VerTexType adjvex; //最小代价边依附的 U 中的顶点
	ArcType lowcost; //最小代价的边的权值
}closedge[MVNum];//记录从 U 到 V-U 具有最小代价的边
void MiniSpanTree_Prim(AMGraph G, VerTexType u)
{
	int i, j, k, min;
	k = LocateVex(G, u);
	for (j = 0; j < G.vexnum; ++j) //辅助数组初始化
		if (j != k)
		{
			closedge[j].adjvex = u;
			closedge[j].lowcost = G.arcs[k][j];
		}
	closedge[k].lowcost = 0;
	//求当前最小边
	for (i = 1; i < G.vexnum; ++i)
	{
		min = MAXINT; //初始化最小权值为无穷大
		j = 1;
		while (j < G.vexnum)
		{
			if (closedge[j].lowcost != 0 && closedge[j].lowcost < min)
			{ //如果权值不为 0,且权值小于 min
				min = closedge[j].lowcost; //则让当前权值成为最小值
				k = j; //将当前最小值的下标存入 k
			}
			j++;
		}
		//k = minimum(closedge);//以上是求解 minimum 的过程
		cout << "(" << closedge[k].adjvex << "," << G.vexs[k] << ")";
		closedge[k].lowcost = 0;
		for (j = 0; j < G.vexnum; ++j)
			if (G.arcs[k][j] < closedge[j].lowcost)
			{
				closedge[j].adjvex = G.vexs[k];
				closedge[j].lowcost = G.arcs[k][j];
			}
	}
	cout << endl;
}
void menu()
{
	cout << endl;
	cout << "----------图的存储结构、遍历及应用-----------" << endl;
	cout << "1.创建无向网" << endl;
	cout << "2.深度优先遍历" << endl;
	cout << "3.广度优先遍历" << endl;
	cout << "4.求最小生成树" << endl;
	cout << "5.两个节点是否连接" << endl;
	cout << "6.退出" << endl;
	cout << "-----------------------------------------------" << endl;
}
int  main()
{
	AMGraph g;
	menu();
	char u;
	bool flag = true;
	int opnum;
	while (flag)
	{
		cout << "请输入您要进行的操作：";
		cin >> opnum;
		switch (opnum)
		{
		case 1:
			CreateUDN(g);
			break;
		case 2:
			cout << "\n 深度优先遍历序列为：";
			DFSTraverse(g);
			cout << endl;
			break;
		case 3:
			cout << "\n 广度优先遍历序列为：";
			BFSTraverse(g);
			cout << endl;
			break;
		case 4:
			cout << "prim 算法请输入起始顶点：" << endl;
			cin >> u;
			MiniSpanTree_Prim(g, u);
			break;
		case 5:
			cout << "请输入两个顶点：" << endl;
			char v;
			cin >> u >> v;
			Link(g, v, u);
			break;


		case 6:
			flag = false;
			break;
		}
	}
	return 0;
}