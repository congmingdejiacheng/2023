#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
using namespace std;
int main(void)
{
	int i;
	int len;
	int nodes[] = { 3,2,1,4,5,6,7,16,15,14,13,12,11,10,8,9 };
	len = sizeof(nodes) / sizeof(nodes[0]);

	PNode root = NULL;
	for (i = 0; i < len; i++)
	{
		root = avltree_insert_node(root, nodes[i]);
	}

	printf("\npreorder:\n");
	preorder(root);

	printf("\ninorder:\n");
	inorder(root);

	printf("\npreorder:\n");
	postorder(root);

	printf("\nroot's deep: %d\n", DEEP(root));
	printf("\nminimum: %d\n", avltree_get_min(root)->val);
	printf("\nmaxmum: %d\n", avltree_get_max(root)->val);

	printf("-----------delete node start-------\n");
	avltree_delete_node_by_val(root, 12);
	printf("\npreorder:\n");
	preorder(root);
	printf("\n");

	avltree_delete_node_by_val(root, 2);
	printf("\npreorder:\n");
	preorder(root);
	printf("\n");

	printf("-----------delete node end-------\n");


	printf("\ndesotry avl tree\n");
	avltree_destory(root);

	return 0;
}
————————————————
版权声明：本文为CSDN博主「mrbone9」的原创文章，遵循CC 4.0 BY - SA版权协议，转载请附上原文出处链接及本声明。
原文链接：https ://blog.csdn.net/mrbone9/article/details/123389078