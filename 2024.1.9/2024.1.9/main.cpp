#include <iostream>
#include <queue>
#include <unordered_map>
#include <algorithm>

using namespace std;

// 哈夫曼树节点
struct Node {
    char data;
    int frequency;
    Node* left;
    Node* right;

    Node(char d, int f) : data(d), frequency(f), left(nullptr), right(nullptr) {}
};

// 比较函数，用于构建优先队列
struct Compare {
    bool operator()(Node* a, Node* b) {
        return a->frequency > b->frequency;
    }
};

// 建立哈夫曼树
Node* buildHuffmanTree(unordered_map<char, int> frequencies) {
    priority_queue<Node*, vector<Node*>, Compare> pq;

    for (auto it = frequencies.begin(); it != frequencies.end(); ++it) {
        Node* newNode = new Node(it->first, it->second);
        pq.push(newNode);
    }

    while (pq.size() > 1) {
        Node* left = pq.top();
        pq.pop();
        Node* right = pq.top();
        pq.pop();

        // 创建新节点作为内部节点，并设置其频率为两个子节点频率之和
        Node* newNode = new Node('\0', left->frequency + right->frequency);
        newNode->left = left;
        newNode->right = right;

        pq.push(newNode);
    }

    return pq.top();
}
//先序遍历哈夫曼树
void preordertraversal(Node* node) {
    if (node == nullptr) {
        return;
    }
    cout << node->data;
    preordertraversal(node->left);
    preordertraversal(node->right);
}
// 生成哈夫曼编码
void generateHuffmanCodes(Node* node, string code, unordered_map<char, string>& huffmanCodes) {
    if (node == nullptr) {
        return;
    }

    if (node->data != '\0') {
        huffmanCodes[node->data] = code;
    }

    generateHuffmanCodes(node->left, code + "0", huffmanCodes);
    generateHuffmanCodes(node->right, code + "1", huffmanCodes);
}

// 哈夫曼编码
string huffmanEncode(string text, unordered_map<char, string> huffmanCodes) {
    string encodedText;
    for (char c : text) {
        encodedText += huffmanCodes[c];
    }
    return encodedText;
}

// 哈夫曼译码
string huffmanDecode(string encodedText, Node* root) {
    Node* current = root;
    string decodedText;

    for (char c : encodedText) {
        if (c == '0') {
            current = current->left;
        }
        else {
            current = current->right;
        }

        if (current->data != '\0') {
            decodedText += current->data;
            current = root;
        }
    }

    return decodedText;
}

int main() {
    string text;
    int n;
    char c1;
    cout << "请输入节点个数" << endl;
    cin >> n;

    // 计算字符频率
    unordered_map<char, int> frequencies;
    for (int i = 0; i < n; i++)
    {
        cout << "请输入第" << i + 1 << "节点和权值" << endl;
        cin >> c1;
        text += c1;
        cin >> frequencies[c1];
    }


    // 构建哈夫曼树
    Node* huffmanTree = buildHuffmanTree(frequencies);
    cout << "先序遍历哈夫曼树" << endl;
    preordertraversal(huffmanTree);
    cout << endl;
    // 生成哈夫曼编码
    unordered_map<char, string> huffmanCodes;
    generateHuffmanCodes(huffmanTree, "", huffmanCodes);

    // 打印字符的哈夫曼编码
    cout << "Huffman Codes:" << endl;
    for (auto it = huffmanCodes.begin(); it != huffmanCodes.end(); ++it) {
        cout << it->first << ": " << it->second << endl;
    }
    cout << endl;

    // 哈夫曼编码
    string encodedText = huffmanEncode(text, huffmanCodes);
    cout << "Encoded Text(译码的测试示例): " << encodedText << endl;

    // 哈夫曼译码
    string tex;
    string decodedText = huffmanDecode(encodedText, huffmanTree);
    cout << "Decoded Text: " << decodedText << endl;

    // 释放内存
    // ...

    return 0;
}
