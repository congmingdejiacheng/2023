#pragma once
#include<assert.h>
template<class K, class V>
struct AVLTreeNode
{
	AVLTreeNode<K, V>* _left;
	AVLTreeNode<K, V>* _right;
	AVLTreeNode<K, V>* _parent;
	pair<K, V> _kv;
	int _bf;
	AVLTreeNode(const pair<K, V>& kv)
		:_left(nullptr)
		,_right(nullptr)
		,_parent(nullptr)
		,_kv(kv)
		,_bf(0)
	{}
};
template<class K,class V>
class AVLTree
{
	typedef AVLTreeNode<K, V> Node;
public :
	bool Insert(const pair<K, V>& kv)
	{
		if (_root == nullptr)
		{
			_root = new Node(kv);
			return true;
		}

		Node* parent = nullptr;
		Node* cur = _root;

		while (cur)
		{
			if (cur->_kv.first < kv.first)
			{
				parent = cur;
				cur = cur->_right;
			}
			else if (cur->_kv.first > kv.first)
			{
				parent = cur;
				cur = cur->_left;
			}
			else
			{
				return false;
			}
		}

		cur = new Node(kv);
		if (parent->_kv.first < kv.first)
		{
			parent->_right = cur;
			cur->_parent = parent;
		}
		else
		{
			parent->_left = cur;
			cur->_parent = parent;
		}

		while (parent)
		{
			if (cur == parent->_left)
			{
				parent->_bf--;
			}
			else
			{
				parent->_bf++;
			}

			if (parent->_bf == 0)
			{
				break;
			}
			else if (parent->_bf == 1 || parent->_bf == -1)
			{
				cur = parent;
				parent = parent->_parent;
			}
			else if (parent->_bf == 2 || parent->_bf == -2)
			{
				if (parent->_bf == 2 && cur->_bf == 1)
				{
					RotateL(parent);
				}
				else if (parent->_bf == -2 && cur->_bf == -1)
				{
					RotateR(parent);
				}
				else if (parent->_bf == 2 && cur->_bf == -1)
				{
					RotateRL(parent);
				}
				else if (parent->_bf == -2 && cur->_bf == 1)
				{
					RotateLR(parent);
				}

				// 1、旋转让这颗子树平衡了
				// 2、旋转降低了这颗子树的高度，恢复到跟插入前一样的高度，所以对上一层没有影响，不用继续更新
				break;
			}
			else
			{
				assert(false);
			}
		}

		return true;
	}

	//bool Insert(const pair<K, V>& kv)
	//{
	//	if (_root == nullptr)//根节点的添加
	//	{
	//		_root = new Node(kv);
	//			return true;
	//	}
	//	Node* parent = nullptr;
	//	Node* cur = _root;
	//	while (cur)//寻找AVL树添加的位置
	//	{
	//		if (cur->_kv.first < kv.first)
	//		{
	//			parent = cur;
	//			cur = cur->_right;
	//		 }
	//		else if (cur->_kv.first > kv.first)
	//		{
	//			parent = cur;
	//			cur = cur->_left;
	//		}
	//		else
	//		{
	//			return false;
	//		}
	//	}
	//	cur = new Node(kv);//创造节点并链接上
	//	//if (parent->_left == cur)
	//	if(parent->_kv.firt>kv.first)
	//	{
	//		cur->_parent = parent;
	//		parent->_left = cur;
	//	}
	//	else
	//	{
	//		cur->_parent = parent;
	//		parent->_right = cur;
	//	}
	//	while (parent)
	//	{
	//		//修改当前节点和父节点的平衡因子
	//		if (cur == parent->_left)
	//		{
	//			parent->_bf--;
	//		}
	//		else
	//		{
	//			parent->_bf++;
	//		}
	//		//通过父节点的平衡因子来修改上面的平衡因子；
	//		if (parent->_bf == 0)
	//		{
	//			break;
	//		}
	//		else if (parent->_bf == 1 || parent->_bf == -1)
	//		{
	//			cur = parent;
	//			parent = cur->_parent;
	//		}
	//		else if (parent->_bf == 2 || parent->_bf == -2)
	//		{
	//			if (parent->_bf == 2&&cur->_bf==1)
	//			{
	//				RotateL(parent);
	//			}
	//			else if (parent->_bf == -2 && cur->_bf == -1)
	//			{
	//				RotateR(parent);
	//			}
	//			else if (parent->_bf == 2 && cur->_bf == -1)
	//			{
	//				RotateRL(parent);
	//			}
	//			else if (parent->bf == -2 && cur->_bf == 1)
	//			{
	//				RotateLR(parent);
	//			}
	//			break;
	//		}
	//		else
	//		{
 //            assert(false);
	//		}
	//		
	//	}
	//	return true;
	//}

	void RotateL(Node* parent)
	{
		
		Node* subR = parent->_right;
		Node* subRL = subR->_left;
		parent->_right = subRL;
		subR->_left = parent;
		Node* parentParent = parent->_parent;
        parent->_parent = subR;

		if (subRL)//这里判断rlchild是否为空
		{
			subRL->_parent = parent;
		}
		
		
		if (_root == parent)
		{
			_root = subR;
			subR->_parent = nullptr;
		}
		else
		{
			if (parentParent->_left == parent)
			{
				parentParent->_left = subR;
			}
			else
			{
				parentParent->_right = subR;
			}
		
			subR->_parent = parentParent;

		}
				parent->_bf = subR->_bf = 0;
//这里的目的就是将者里的2和1的节点干成0；
	}

	void RotateR(Node* pParent)
	{
		Node* ppParent = pParent->_parent;
		Node* lChild = pParent->_left;
		Node* lrChild = pParent->_left->_right;
		lChild->_right = pParent;
		pParent->_left = lrChild;

		if (lrChild)//这里判断rlchild是否为空
			lrChild->_parent = pParent;
		pParent->_parent = lChild;
		if (_root == pParent)
		{
			_root = lChild;
			lChild->_parent = nullptr;
		}
		else
		{
			if (ppParent->_right == pParent)
			{
				ppParent->_right = lChild;
			}
			else
			{
				ppParent->_left = lChild;
			}
			lChild->_parent = ppParent;

		}
		pParent->_bf = lChild->_bf = 0;//这里的目的就是将者里的2和1的节点干成0；
	}

	void RotateRL(Node* pParent)
	{
	
		Node* subR = pParent->_right;
		Node* subRL = subR->_left;
		RotateR(pParent->_right);
		RotateL(pParent);
		int bf = subRL->_bf;
		if (bf == -1)
		{
			subRL->_bf = 0;
			subR->_bf = 1;
			pParent->_bf = 0;
		}
		else if (bf == 1)
		{
			subRL->_bf = 0;
			subR->_bf = 0;
			pParent->_bf = -1;
		}
		else if (bf == 0)
		{
			subRL->_bf = 0;
			subR->_bf = 0;
			pParent->_bf = 0;
		}
		else
		{
			assert(false);
		}
	}

	void RotateLR(Node* pParent)
	{	Node* subL = pParent->_left;
		Node* subLR = subL->_right;
		RotateL(pParent->_left);
		RotateR(pParent);
	
		int bf = subLR->_bf;
		if (bf == -1)
		{
			subLR->_bf = 0;
			subL->_bf = 0;
			pParent->_bf = -1;

		}
		else if (bf == 1)
		{
			subLR->_bf = 0;
			subL->_bf = -1;
			pParent->_bf = 0;
		}
		else if (bf == 0)
		{
			subLR->_bf = 0;
			subL->_bf = 0;
			pParent->_bf = 0;
		}
		else
		{
			assert(false);
		}

	}
	void InOrder()
	{
		_InOrder(_root);
		cout << endl;
	}

	void _InOrder(Node* root)
	{
		if (root == nullptr)
			return;

		_InOrder(root->_left);
		cout << root->_kv.first << " ";
		_InOrder(root->_right);
	}
	bool IsBalance()
	{
		return _IsBalance(_root);
	}
	//先想空
	//在使用递归
	int _Height(Node* root)
	{
		if (root == nullptr)
			return 0;
		int leftHeight = _Height(root->_left);
		int rightHeight = _Height(root->_right);
		return leftHeight > rightHeight ? leftHeight + 1 : rightHeight + 1;
	}
	//int _Height(Node* root)
	//{
	//	if (root == nullptr)
	//		return 0;

	//	int leftHeight = _Height(root->_left);
	//	int rightHeight = _Height(root->_right);

	//	return leftHeight > rightHeight ? leftHeight + 1 : rightHeight + 1;
	//}

	bool _IsBalance(Node* root)
	{
		if (root == nullptr)
		{
			return true;
		}
		int leftHeight = _Height(root->_left);
		int rightHeight = _Height(root->_right);
		int leftHeight = _Height(root->_left);
		int rightHeight = _Height(root->_right);
		if (rightHeight - leftHeight != root->_bf)
		{
			cout << root->_kv.first << "平衡因子异常" << endl;
			return false;
		}
		return abs(rightHeight - leftHeight) < 2
			&& _IsBalance(root->_left)
			&& _IsBalance(root->_right);

	}
	//bool _IsBalance(Node* root)
	//{
	//	if (root == nullptr)
	//		return true;

	//	int leftHeight = _Height(root->_left);
	//	int rightHeight = _Height(root->_right);
	//	if (rightHeight - leftHeight != root->_bf)
	//	{
	//		cout << root->_kv.first << "平衡因子异常" << endl;
	//		return false;
	//	}

	//	return abs(rightHeight - leftHeight) < 2
	//		&& _IsBalance(root->_left)
	//		&& _IsBalance(root->_right);
	//}
private:
	Node* _root = nullptr;
};


