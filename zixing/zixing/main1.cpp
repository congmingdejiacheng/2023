#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<stdlib.h>
#include<conio.h>
#include<string.h>
#include<windows.h>
#include <malloc.h>
#include<dos.h>
#define len sizeof(struct dorminfo)
#define Max 100 
int a[Max] = { 0 };
int length;
void showmenu();
void processmenu();
void create();
void display();
void disbed();
void modify();
void del();
void save();
void read();
typedef struct
{
	int dormno;
	int bedno;
	int isstay;
}bedinfo;
struct dorminfo
{
	bedinfo bed;
	int stuno;
	char name[20];
	char stucla[20];
	int year;
	int month;
	struct dorminfo* next;
};
struct dorminfo* head = NULL, * p1, * p2, * p3;

void showmenu()
{
	printf("\n\n\n\t\t 欢迎进入学生宿舍管理系统\n");
	printf("\t\t*********************************\n");
	printf("\t\t1.输入床位信息\n");
	printf("\t\t2.根据学号,显示学生住宿信息\n");
	printf("\t\t3.根据宿舍号,显示住宿信息\n");
	printf("\t\t4.根据宿舍号、床位号,修改住宿信息\n");
	printf("\t\t5.根据宿舍号、床位号,删除住宿信息\n");
	printf("\t\t6.保存学生宿舍信息\n");
	printf("\t\t7.从文件读入学生信息\n");
	printf("\t\t8.退出 \n");
	printf("\t\t********************************\n");
}
void processmenu()
{
	int caidan;
	printf("请输入您的选项<1~8>:");
	scanf("%d", &caidan);
	if (caidan > 8 || caidan < 0)
	{
		printf("对不起，您输入的选项有误，请重新输入!");
		scanf("%d", &caidan);
	}
	switch (caidan)
	{
	case 1:create(); break;
	case 2:display(); break;
	case 3:disbed(); break;
	case 4:modify(); break;
	case 5:del(); break;
	case 6:save(); break;
	case 7:read(); break;
	case 8:exit(0);
	}
}
void create()
{
	int j;
	char ch;
	length = 0;
	p1 = (struct dorminfo*)malloc(len);
	if (head == NULL)
		head = p1;
	printf("开始输入床位信息....\n");
	Sleep(500);
	do
	{
		p2 = (struct dorminfo*)malloc(len);
		printf("请输入宿舍号:");
		scanf("%d", &p2->bed.dormno);
		printf("请输入床号:");
		scanf("%d", &p2->bed.bedno);
		printf("是否有人居住(1/0):");
		scanf("%d", &p2->bed.isstay);
		printf("请输入学生学号:");
		scanf("%d", &p2->stuno);
		a[length] = p2->stuno;
		if (length > 1)
		{
			for (j = 1; j < length; j++)
			{
				if (a[length] == a[j])
				{
					printf("该床位号已存在，请重新输入:");
					scanf("%d", &p2->bed.bedno);
				}
			}
		}
		printf("请输入学生姓名:");
		scanf("%s", p2->name);
		printf("请输入学生班级:");
		scanf("%s", p2->stucla);
		printf("请输入学生入住时间(年 月)<如2015 5>:");
		scanf("%d%d", &p2->year, &p2->month);
		if (p2->year < 1000 || p2->year>9999 || p2->month > 12 || p2->month < 1)
		{
			printf("对不起，输入错误，请重新输入!\n");
			scanf("%d%d\n", &p2->year, &p2->month);
		}
		length++;
		p1->next = p2;
		p2->next = NULL;
		p1 = p1->next;
		printf("第%d个住宿信息创建成功!\n", length);
		Sleep(300);
		fflush(stdin);
		printf("是否继续添加住宿信息?<y/Y>");
		ch = getchar();
	} while (ch == 'y' || ch == 'Y');
}
void display()
{
	int flag = 0, No;
	p3 = head->next;
	printf("请输入学号:");
	scanf("%d", &No);
	while (p3 != NULL)
	{
		if (p3->stuno == No)
		{
			printf("\n住宿信息如下:");
			printf("\n宿舍号:%d", p3->bed.dormno);
			printf("\n床位号:%d", p3->bed.bedno);
			printf("\n是否有人入住:%d", p3->bed.isstay);
			printf("\n学生学号:%d", p3->stuno);
			printf("\n学生姓名:%s", p3->name);
			printf("\n学生班级:%s", p3->stucla);
			printf("\n入住时间为:%d年%d月\n", p3->year, p3->month);
			flag = 1;
		}
		p3 = p3->next;
	}
	if (!flag)
		printf("没有找到该学生住宿信息！\n");
}
void disbed()
{
	int flag = 0, DormNo;
	p3 = head->next;
	printf("请输入宿舍号:");
	scanf("%d", &DormNo);
	while (p3 != NULL)
	{
		if (p3->bed.dormno == DormNo)
		{
			printf("\n住宿信息如下:");
			printf("\n宿舍号:%d", p3->bed.dormno);
			printf("\n床位号:%d", p3->bed.bedno);
			printf("\n是否有人入住:%d", p3->bed.isstay);
			printf("\n学生学号:%d", p3->stuno);
			printf("\n学生姓名:%s", p3->name);
			printf("\n学生班级:%s", p3->stucla);
			printf("\n入住时间为:%d年%d月\n", p3->year, p3->month);
			flag = 1;
		}
		p3 = p3->next;
	}
	if (!flag)
		printf("没有找到该学生住宿信息！\n");
}
void modify()
{
	struct dorminfo* p;
	int DormNo, BedNo, flag;
	int Dormno, Bedno, Isstay, Stuno, Year, Month;
	char Name[20], Stucla[20];
	if (head == NULL) {
		printf("住宿信息为空,不能修改,按任意键返回...\n");
		getch();
		system("cls");
		return;
	}
	p1 = p3 = head;
	printf("请输入宿舍号、床位号:");
	scanf("%d %d", &DormNo, &BedNo);
	while (p3 != NULL)
	{
		if (p3->bed.dormno == DormNo && p3->bed.bedno == BedNo)
		{
			printf("已找到要修改的宿舍号、床位号!\n");
			flag = 1;
			p = p3;
			Sleep(1000);
		}
		p3 = p3->next;
	}
	if (flag)
	{
		printf("请输入修改后的宿舍号:");
		scanf("%d", &Dormno);
		printf("请输入修改后的床号:");
		scanf("%d", &Bedno);
		printf("是否有人居住(1/0):");
		scanf("%d", &Isstay);
		printf("请输入修改后的学生学号:");
		scanf("%d", &Stuno);
		while (p1 != NULL)
		{
			if (p1->stuno == Stuno) {
				printf("该床位号已存在，请重新输入:");
				scanf("%d", &Stuno);
			}
			p1 = p1->next;
		}
		printf("请输入修改后的学生姓名:");
		scanf("%s", Name);
		printf("请输入修改后的学生班级:");
		scanf("%s", Stucla);
		printf("请输入修改后的学生入住时间(年月)<如2015 5>:");
		scanf("%d%d", &Year, &Month);
		if (Year < 1000 || Year>9999 || Month > 12 || Month < 1)
		{
			printf("对不起,输入错误,请重新输入!\n");
			scanf("%d%d", &Year, &Month);
		}
		p->bed.dormno = Dormno;
		p->bed.bedno = Bedno;
		p->bed.isstay = Isstay;
		p->stuno = Stuno;
		strcpy(p->name, Name);
		strcpy(p->stucla, Stucla);
		p->year = Year;
		p->month = Month;
		printf("修改成功,信息如下:\n");
		printf("-宿舍号--床号--有无人入住--学号--姓名--班级--入住时间(年月)");
		printf("\n");
		printf("%6d%5d%8d%9d%7s%8s%9d%2d\n", p->bed.dormno, p->bed.bedno, p->bed.isstay, p->stuno, p->name, p->stucla, p->year, p->month);
	}
	else
	{
		printf("没有找到该宿舍号与床号信息,按任意键返回...\n");
		getch();
		system("cls");
		return;
	}
}
void del()
{
	int DormNo, BedNo;
	p1 = head, p2 = head->next;
	printf("请输入宿舍号、床位号:");
	scanf("%d %d", &DormNo, &BedNo);
	while (p2 != NULL)
	{
		if (p2->bed.dormno == DormNo && p2->bed.bedno == BedNo)
		{
			p1->next = p2->next;
			free(p2);
			length--;
			printf("删除成功!\n");
			system("cls");
			return;
		}
		p2 = p2->next;
		p1 = p1->next;
	}
	if (p1 == NULL)
		printf("找不到要删除的宿舍号、床位号!\n");
}
void save()
{
	FILE* fp1;
	if (head == NULL)
	{
		printf("\n，没有找到该宿舍号与床号信息,按任意键返回....");
		getch();
		system("cls");
		return;
	}
	if ((fp1 = fopen("学生宿舍管理系统.txt", "w")) == NULL)
	{
		printf("\n信息无法保存，按任意键返回....");
		getch();
		system("cls");
		return;
	}
	p1 = head->next;
	printf("正在保存文件...\n");
	Sleep(800);
	fputs(" 宿舍号 床号 有无人入住 学号 姓名 班级 入住时间(年月) :\n", fp1);
	while (p1 != NULL)
	{
		fprintf(fp1, "%6d%5d%8d%7d%6s%4s%7d%2d\n", p1->bed.dormno, p1->bed.bedno, p1->bed.isstay, p1->stuno, p1->name, p1->stucla, p1->year, p1->month);
		p1 = p1->next;
	}
	fclose(fp1);
	printf("文件保存成功!\n");
	printf("按任意键返回...\n");
	getch();
	system("cls");
	return;
}
void read()
{
	FILE* fp2;
	char ch;
	if ((fp2 = fopen("学生宿舍管理系统.txt", "r")) == NULL)
	{
		printf("文件无法打开!\n");
		exit(0);
	}
	printf("正在读入文件...\n");
	Sleep(1000);
	ch = fgetc(fp2);
	while (!feof(fp2))
	{
		putchar(ch);
		ch = fgetc(fp2);
	}
	printf("\n");
	fclose(fp2);
	printf("文件读入成功!\n");
}
int main()
{
	while (1)
	{
		showmenu();
		processmenu();
		system("pause");
		system("cls");
		
	}return 0;
}

