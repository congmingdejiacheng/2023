#pragma once
#include<vector>
#include<map>
#include<assert.h>
namespace Matrix
{
	template<class W>
	struct Edge
	{
		int _dsti;
		W _w;
		Edge<W>* next;
		Edge(int dsti, const W& w)
			:_dsti(dsti)
			, _w(w)
			, next(nullptr)
		{}
	};
	//首先定义节点和边的类型然后定义最大值和有无向图
	template<class V, class W,bool Direction = false>
	class Graph
	{
		typedef Edge<W> Edge;
	public:
		//图的创建：
		//IO输入
		//文件读取
		//手动添加边
		Graph(const V* a, size_t n)
		{
			_vertexs.reserve(n);
			for (size_t i = 0; i < n; i++)
			{
				//注意这里使用的reserve是先预测创建n个节点；还没有创建；所以不可以使用【】；
				_vertexs.push_back(a[i]);
				_indexMap[a[i]] = i;//映射
			}
			arr.resize(n, nullptr);
			

		}

		//Graph(const V* a, size_t n)
	 //   {
		//_vertexs.reserve(n);
		//for (size_t i = 0; i < n; ++i)
		//{
		//	_vertexs.push_back(a[i]);
		//	_indexMap[a[i]] = i;
		//}

		//_matrix.resize(n);
		//for (size_t i = 0; i < _matrix.size(); ++i)
		//{
		//	_matrix[i].resize(n, MAX_W);
		//}
	 //    }
		//这里不可以直接使用map而是要判断v是否在集合中
		size_t GetVertexsIndex(const V& v)
		{
			auto it = _indexMap.find(v);
			if (it != _indexMap.end())
			{
				return it->second;
			}
			else
			{
				assert(false);
				return -1;
			}
		}
		void ADDEdge(const V& src, const V& dst, const W& w)
		{
			int i = GetVertexsIndex(src);
			int j = GetVertexsIndex(dst);
			Edge* e1 = new Edge(j, w);//定义一个在该函数用完后就销毁
			e1->next = arr[i];
			arr[i] = e1;
			//false是无向图
			if (Direction == false)
			{
				Edge* e2 = new Edge(i, w);
				e2->next = arr[j];
				arr[j] = e2;

			}

		}
		void Print()
		{
			//顶点行
			for (size_t i = 0; i < _vertexs.size(); i++)
			{
				cout << "[" << i << "]" << "->" << _vertexs[i] << endl;
			}
			//打印完以后全部换行
			cout << endl;
			//表
			for (size_t i = 0; i < arr.size(); i++)
			{
				cout << "[" << _vertexs[i] << "]" << "->";
				Edge* cur = arr[i];
				while (cur)
				{
					cout << "[" << _vertexs[cur->_dsti] << "]" << "->";
					cur = cur->next;
				}
				cout << "nullptr" << endl;
			}
			
		}



	private:
		vector<V>_vertexs;
		map<V, int>_indexMap;
		vector<Edge*>arr;
	};

}
void text1()
{
	const char* str = "abcdefghi";
	Matrix::Graph<char, int> g(str, strlen(str));
	g.ADDEdge('a', 'b', 1);
	g.ADDEdge('a', 'c', 3);
	g.ADDEdge('a', 'd', 4);
	g.ADDEdge('b', 'c', 5);
	g.ADDEdge('c', 'd', 6);
	g.ADDEdge('d', 'e', 8);
	g.Print();


}
