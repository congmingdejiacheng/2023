#define _CRT_SECURE_NO_WARNINGS 1
#include "sort.h"
void PrintArray(int* a, int n)
{
	for (int i = 0; i < n; ++i)
	{
		printf("%d ", a[i]);
	}
	printf("\n");
}
void InsertSort(int* a, int n)

{
	for (int i =1 ; i < n; i++)

	{
		int end = i-1;
		int tmp = a[i];
		while (end>= 0)
		{
			if (a[end] >tmp)
			{
				
				a[end+1] = a[end];
				end--;
			}
			else
			{
				
				break;

			}
		}
		a[end + 1] = tmp;
	}
}
void BubbleSort(int* a, int n)
{
	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < n-i-1; j++)
		{
			if (a[j] > a[j + 1])
			{
				int tmp = a[j + 1];
				a[j + 1] = a[j];
				a[j] = tmp;

			}
		}
	}
}
void ShellSort(int* a, int n)
{
	int gap = n;
	while(gap>1)
	{
		gap = gap / 3 + 1;
		for (int i = 1; i < n ; ++i)
		{
			int end = i-gap;
			int tmp = a[i];
			while (end >= 0)
			{
				if (a[end] > tmp)
				{
					a[end + gap] = a[end];
					end -= gap;
				}
				else
				{
					break;
				}
			}
			a[end + gap] = tmp;

		}
	}
	
}
void Swap(int* p1, int* p2)
{
	int tmp = *p1;
	*p1 = *p2;
	*p2 = tmp;
}
void SelectSort(int* a, int n)
{
	int begin = 0, end = n - 1;
	while (begin < end)
	{
		int max = begin, mini = begin;
		for (int i=begin; i <= end; i++)
		{
			if (a[i] > a[max])
			{
				max = i;
			}
			if (a[i] < a[mini])
			{
				mini = i;
			}
	   }
		Swap(&a[begin], &a[mini]);
		if (begin == max)///这里始终好点

		{
			max = mini;
		}
		Swap(&a[end], &a[max]);
		++begin;
		--end;
	}
}
//void AdjustDown(int* a, int parent,int n)
//{
//	int child = parent * 2 + 1;
//	while (child<n)
//	{
//	  
//	    if (child+1 < n && a[child] < a[child + 1])
//	    {
//		child++;
//	    }
//
//		if (a[parent] < a[child])
//		{
//			Swap(&a[parent], &a[child]);
//			parent = child;
//			child = parent * 2 + 1;
//		}
//		else
//		{
//			break;
//		}
// 
//
//	
//	}
//	
//}
void AdjustDown(int* a, int n ,int parent)
{
	int child = parent * 2 + 1;
	while (child < n)
	{
		// 选出左右孩子中小/大的那个
		if (child + 1 < n
			&& a[child + 1] > a[child])
		{
			++child;
		}

		if (a[child] > a[parent])
		{
			Swap(&a[parent], &a[child]);
			parent = child;
			child = parent * 2 + 1;
		}
		else
		{
			break;
		}
	}
}

void HeapSort(int* a, int n)
{
	//1。就在原本数组上建堆
	//向下调整建堆：
	//              从最后一个元素的父节点开始进行向下调整建堆；
	//			    循环到头
    // AdjustDown主要的方法：内部分支语句外部循环语句
	//后使用循环使用向下调整建堆；
	for (int i = (n - 2) / 2; i >= 0; i--)
	{
		AdjustDown(a, n,i);
	}
	//最后使用循环加向下调整进行排序；
	for (int i = n-1; i > 0; i--)
	{
		Swap(&a[0], &a[i]);
		AdjustDown(a,i, 0);
	}
	
}

//int PartSort(int* a, int left, int right)
//{
//	int keyi = left;
//	while (left < right)
//	{	while (left < right &&a[right] >= a[keyi])
//		{
//			right--;
//		}
//		while (left < right &&a[left] <= a[keyi])
//		{
//			left++;
//		}
//	
//		Swap(&a[left], &a[right]);
//	}
//	Swap(&a[keyi], &a[left]);
//	return left;
//}
//int PartSort(int* a, int left, int right)
//{
//	int keyi = a[left];
//	int hole = left;
//	while (left < right)
//	{  
//		while (left < right && a[right] >= keyi)
//		{
//			right--;
//		}
//		a[hole] = a[right];
//		hole = right;
//		 while (left < right && a[left] <= keyi)
//		{
//			left++;
//		}
//		a[hole] = a[left];
//		hole = left;
//
//	}
//	a[hole] = keyi;
//	return hole;
//}

int PartSort(int* a, int left, int right)
{
	int keyi = left;
	int prev = left; int  cur = left + 1;
	while (cur<=right)
	{  
		if (a[cur]< a[keyi]&& ++prev!=cur)
		{		
		
			Swap(&a[prev],&a[cur]);
			
		}
		
		cur++;
	}
	Swap(&a[prev], &a[keyi]);
	keyi = prev;
	return keyi;
}


void QuickSort(int *a,int begin,int end)
{
	if (begin >= end)
	{
		return;
	}
	int keyi=PartSort(a, begin, end);
	QuickSort(a, begin, keyi - 1);
	QuickSort(a, keyi + 1,end);

}
