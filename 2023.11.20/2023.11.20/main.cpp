#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    vector<int> postorderTraversal(TreeNode* root)
    {
        TreeNode* cur = root;
        stack<TreeNode*>arr;
        vector<int> res;
        TreeNode* prev = nullptr;
        if (cur == nullptr)
        {
            return res;
        }
        arr.emplace(cur);
        cur = cur->left;
        while (!arr.empty())
        {
            while (cur != nullptr)
            {
                arr.emplace(cur);
                cur = cur->left;
            }
            cur = arr.top();
            arr.pop();
            if (cur->right == nullptr || cur->right == prev)
            {
                res.emplace_back(cur->val);
                prev = cur;
                cur = nullptr;
            }
            else
            {
                arr.emplace(cur);
                cur = cur->right;
            }

        }   return res;
    }



};
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    vector<int> inorderTraversal(TreeNode* root) {
        stack<TreeNode*> arr;
        vector<int>ptr;
        if (root == nullptr)
        {
            return ptr;
        }


        while (root != nullptr || !arr.empty())
        {
            while (root != nullptr)
            {
                arr.push(root);
                root = root->left;
            }
            root = arr.top();
            arr.pop();


            ptr.push_back(root->val);
            root = root->right;


        }
        return ptr;
    }
};

