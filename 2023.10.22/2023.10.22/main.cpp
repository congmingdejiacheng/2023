#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<string>
#include<deque>
#include<stack>
#include<queue>
//#include<algorithm>
using namespace std;

#include"Stack.h"
#include"Queue.h"
void test_queue()
{
    bit::queue<int> q;
    q.push(1);
    q.push(3);
    q.push(2);
    q.push(7);
    q.push(5);

    while (!q.empty())
    {
        cout << q.front() << " ";
        q.pop();
    }
    cout << endl;
}
int main()
{
    test_queue();
    return 0;
}