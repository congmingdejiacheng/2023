#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<vector>
using namespace std;
void duplicateZeros(vector<int>& arr) {
    vector<int>::iterator cur = arr.begin();
    while (cur != arr.end())
    {
        if (*cur == 0)
        {
            cur=arr.insert(cur, 0);
        }
        cur++;
    }
}
int main()
{
    int a[] = { 1,0,3,0,5,6,7,8,0,5,8,0, };
    vector<int> arr(a,a+sizeof(a)/sizeof(int));
    duplicateZeros(arr);
    return 0;
}