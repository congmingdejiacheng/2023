#define _CRT_SECURE_NO_WARNINGS 1
/*BiTreeExam.cpp*/
#include<iostream>
using namespace std;
#define OVERFLOW -2
/*二叉树的二叉链表节点结构*/
typedef struct BiTNode {
	char data;
	struct BiTNode* lChild, * rChild;
} BiTNode, * BiTree; //结点数据元素的类型为字符型
/*使用先序遍历创建二叉树*/
void CreateBiTree(BiTree& T) {
	char ch;
	cin >> ch;
	if (ch == '#')

	{
		T = NULL;
		return;
	}
	else
	{
		T = new BiTNode;
		if (T == NULL) {
			cout << "内存空间不足,程序退出" << endl;
			exit(OVERFLOW);
		}
		T->data = ch; 
		CreateBiTree(T->lChild); 
		CreateBiTree(T->rChild);
	}
}
/*先序遍历*/
void PreOrderTraverse(BiTree T)
{
	if (T)
	{
		cout << T->data << " ";
		PreOrderTraverse(T->lChild);
		PreOrderTraverse(T->rChild);

}
	
	
}
/*中序遍历*/
void InOrderTraverse(BiTree T)
{
	if (T)
	{
		PreOrderTraverse(T->lChild);
		cout << T->data << " ";
		PreOrderTraverse(T->rChild);
	}
	
}
/*后序遍历*/

void PostOrderTraverse(BiTree T)
{
	if (T)
	{
		PreOrderTraverse(T->lChild);
		PreOrderTraverse(T->rChild);
		cout << T->data << " ";
	}
	
}
/*利用递归算法：求二叉树中所有结点值之和*/
int SumNodeVal(BiTree T)
{
	if (T == NULL) return 0;
	else
		return SumNodeVal(T->lChild)+ SumNodeVal(T->rChild)+T->data;
}
/*利用递归算法：求二叉树的深度*/
int Depth(BiTree T)
{
	if (T == nullptr)
	{
		return 0;
	}
	return Depth(T->lChild) + 1;
}
/*利用递归算法：求二叉树中结点的个数*/
int NodeCount(BiTree T)
{
	if (T==NULL)
	{
		return 0;
	}
	return NodeCount(T->lChild) + NodeCount(T->rChild) + 1;
	
}
//其他一些应用算法自己补充
/*菜单*/
void menu()
{
	cout << endl;
	cout << "---------二叉树的基本操作和应用---------" << endl;
	cout << "1.先序创建二叉树" << endl;
	cout << "2.显示先序遍历的结果" << endl;
	cout << "3.显示中序遍历的结果" << endl;
	cout << "4.显示后续遍历的结果" << endl;
	cout << "5.计算二叉树中所有结点值的和" << endl;
	cout << "6.计算二叉树中结点的个数" << endl;
	cout << "7.计算二叉树的深度" << endl;
	cout << "8.退出" << endl;
	cout << "----------------------------------------" << endl;
	cout << endl;
}
int main()
{
	menu();
	BiTree tree=nullptr;
	int opnum;
	bool flag = true;
	while (flag)
	{
		cout << "请您选择要进行的操作：";
		cin >> opnum;
		switch (opnum)
		{
		case 1:
			cout << "以先序序列输入一棵二叉树的结点，左右孩子为空时输入#：\n"; CreateBiTree(tree);
			break;
		case 2:
			cout << "先序序列遍历二叉树顺序为："; PreOrderTraverse(tree);
			cout << "\n";
			break;
		case 3:
			cout << "中序序列遍历二叉树顺序为："; InOrderTraverse(tree);
			cout << "\n";
			break;
		case 4:
			cout << "后序序列遍历二叉树顺序为："; PostOrderTraverse(tree);
			cout << "\n";
			break;
		case 5:
			cout << "二叉树中结点的和为" << SumNodeVal(tree) << endl;
			break;
		case 6:
			cout << "二叉树中结点的个数：" << NodeCount(tree) << endl;
			break;
		case 7:
			cout << "二叉树的高度" << Depth(tree) << endl;
			break;
		case 8:
			flag = false;
			break;
		}
	}
	return 0;
}