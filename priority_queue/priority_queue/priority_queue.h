#pragma once

#include <vector>

namespace bit
{   template<class T,class Container=vector<T>,class Compare=Less>
	class priority_queue
	{
	public:
	    //这里不需要构造函数和拷贝构造函数；
		//原因：私有是vector（自定义类型）
		void adjust_up(int child)
		{
			Compare com;
			int parent = (child - 1) / 2;
			while (child > 0)
			{
				if (com(_con[parent],_con[child]))

				{
					swap(_con[child], _con[parent]);
					child = parent;
					parent = (parent - 1) / 2;
				}
				else
				{
					break;
				}
			}
		}
		void adjust_down()
		{
			Compare com;
			size_t child = parent * 2 + 1;
			while (child < _con.size())
			{
				if (child + 1 < _con.size() && _con[child] < _con[child + 1])//右孩子先判断
				{
					++child;
				}
				if (com(_con[parent],_con[child]))
				{
					swap(_con[child], _con[parent]);
					parent = child;
					child = parent * 2 + 1;
				}
				else
				{
					break;
				}
			}
		}
		void push(const T&x)//这里可以减少值传递的时间；从而提高效率
		{
			_con.push_back(x);
			adjust_up(_con.size() - 1);

		}

		void pop()
		{
			swap(_con[0], _con[_size() - 1]);
			_con.pop_back();
			adjust_down()
		}

	private:
		Container _con;
	};
}