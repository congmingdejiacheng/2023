#define _CRT_SECURE_NO_WARNINGS 1
--add - file = FILE        add given FILE to the archive(useful if its name
    starts with a dash)
    --backup[=CONTROL]     backup before removal, choose version CONTROL
    - C, --directory = DIR        change to directory DIR
    --exclude = PATTERN      exclude files, given as a PATTERN
    --exclude - backups      exclude backup and lock files
    --exclude - caches       exclude contents of directories containing
    CACHEDIR.TAG, except for the tag file itself
    --exclude - caches - all   exclude directories containing CACHEDIR.TAG
    --exclude - caches - under exclude everything under directories containing
    CACHEDIR.TAG
    --exclude - tag = FILE     exclude contents of directories containing FILE,
    except for FILE itself
    --exclude - tag - all = FILE exclude directories containing FILE
    --exclude - tag - under = FILE   exclude everything under directories
    containing FILE
    --exclude - vcs          exclude version control system directories
    - h, --dereference          follow symlinks; archiveand dump the files they
    point to
    --hard - dereference     follow hard links; archiveand dump the files they
    refer to
    - K, --starting - file = MEMBER - NAME
    begin at member MEMBER - NAME when reading the
    archive
    --newer - mtime = DATE     compare date and time when data changed only
    --no - null              disable the effect of the previous --null option
    --no - recursion         avoid descending automatically in directories
    --no - unquote           do not unquote filenames read with - T
    --null - T reads null - terminated names, disable - C
    - N, --newer = DATE - OR - FILE, --after - date = DATE - OR - FILE
    only store files newer than DATE - OR - FILE
    --one - file - system      stay in local file system when creating archive
    - P, --absolute - names       don't strip leading `/'s from file names
    --recursion            recurse into directories(default)
    --suffix = STRING        backup before removal, override usual suffix('~'
        unless overridden by environment variable
        SIMPLE_BACKUP_SUFFIX)
    - T, --files - from = FILE      get names to extract or create from FILE
    --unquote              unquote filenames read with - T(default)
    - X, --exclude - from = FILE    exclude patterns listed in FILE

    File name transformations :

--strip - components = NUMBER   strip NUMBER leading components from file
names on extraction
--transform = EXPRESSION, --xform = EXPRESSION
use sed replace EXPRESSION to transform file
names

File name matching options(affect both exclude and include patterns) :

    --anchored             patterns match file name start
    --ignore - case          ignore case
    --no - anchored          patterns match after any `/' (default for
    exclusion)
    --no - ignore - case       case sensitive matching(default)
    --no - wildcards         verbatim string matching
    --no - wildcards - match - slash   wildcards do not match `/'
    --wildcards            use wildcards(default)
    --wildcards - match - slash   wildcards match `/' (default for exclusion)

    Informative output :

--checkpoint[=NUMBER]  display progress messages every NUMBERth record
(default 10)
--checkpoint - action = ACTION   execute ACTION on each checkpoint
--full - time            print file time to its full resolution
--index - file = FILE      send verbose output to FILE
- l, --check - links          print a message if not all links are dumped
--no - quote - chars = STRING   disable quoting for characters from STRING
--quote - chars = STRING   additionally quote characters from STRING
--quoting - style = STYLE  set name quoting style; see below for valid STYLE
values
- R, --block - number         show block number within archive with each message

--show - defaults        show tar defaults
--show - omitted - dirs    when listing or extracting, list each directory
that does not match search criteria
--show - transformed - names, --show - stored - names
show file or archive names after transformation
--totals[=SIGNAL]      print total bytes after processing the archive;
with an argument - print total bytes when this
SIGNAL is delivered; Allowed signals are : SIGHUP,
SIGQUIT, SIGINT, SIGUSR1and SIGUSR2; the names
without SIG prefix are also accepted
--utc                  print file modification times in UTC
- v, --verbose              verbosely list files processed
--warning = KEYWORD      warning control
- w, --interactive, --confirmation
ask for confirmation for every action

Compatibility options :

-o                         when creating, same as --old - archive; when
extracting, same as --no - same - owner

Other options :

-? , --help                 give this help list
--restrict             disable use of some potentially harmful options
--usage                give a short usage message
--version              print program version

Mandatory or optional arguments to long options are also mandatory or optional
for any corresponding short options.

The backup suffix is `~', unless set with --suffix or SIMPLE_BACKUP_SUFFIX.
The version control may be set with --backup or VERSION_CONTROL, values are :

none, off       never make backups
t, numbered     make numbered backups
nil, existing   numbered if numbered backups exist, simple otherwise
never, simple   always make simple backups

Valid arguments for the --quoting - style option are :

literal
shell
shell - always
c
c - maybe
escape
locale
clocale

* This * tar defaults to :
--format = gnu - f - -b20 --quoting - style = escape --rmt - command = / etc / rmt
--rsh - command = / usr / bin / ssh

Report bugs to <bug - tar@gnu.org>.
[root@VM - 16 - 11 - centos ~]#  ^ C
[root@VM - 16 - 11 - centos ~]# 
